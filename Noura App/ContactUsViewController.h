//
//  ContactUsViewController.h
//  Noura App
//
//  Created by volive solutions on 15/11/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SharedClass.h"
@interface ContactUsViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIBarButtonItem *backBar;
@property (weak, nonatomic) IBOutlet UILabel *aboutUsLabel;
@property (weak, nonatomic) IBOutlet UITextView *aboutTextView;
@property (weak, nonatomic) IBOutlet UIWebView *AboutWebView;

@end
