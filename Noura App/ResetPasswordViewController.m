//
//  ResetPasswordViewController.m
//  Noura App
//
//  Created by volive solutions on 06/10/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "ResetPasswordViewController.h"
#import "SharedClass.h"
#import "HeaderAppConstant.h"
#import "LineTextField.h"
@interface ResetPasswordViewController (){
    SharedClass *obj;

}

@end

@implementation ResetPasswordViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadViewWithCustomDesign];
    // Do any additional setup after loading the view.
    
}

-(void)loadViewWithCustomDesign {

    
    self.navigationItem.title = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Reset Password"];
    obj = [[SharedClass alloc] init];
    _submitBtn.layer.masksToBounds = false;
    _submitBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _submitBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _submitBtn.layer.shadowRadius = 3;
    _submitBtn.layer.shadowOpacity = 0.5;
    
    int selectedLanguage = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    
    if(selectedLanguage == 2){
        
        _createPasswordTF.textAlignment = NSTextAlignmentRight;
        _cnformPasswordTF.textAlignment = NSTextAlignmentRight;
        
    } else  {
        
        _createPasswordTF.textAlignment = NSTextAlignmentLeft;
        _cnformPasswordTF.textAlignment = NSTextAlignmentLeft;
    }
    
    LineTextField *line = [[LineTextField alloc]init];
    
    [line textfieldAsLine:_createPasswordTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"New Password"] myView:self.view];
    [line textfieldAsLine:_cnformPasswordTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Confirm Password"] myView:self.view];
//    _createPasswordTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"New Password"];
//    _cnformPasswordTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Confirm Password"];
    
    [_submitBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Submit"] forState:UIControlStateNormal];


}


- (IBAction)submitBtnAction:(id)sender {
    
    
    
    [self ChangePassword];
    
}

#pragma mark - resetpassword
-(void)ChangePassword{
//http://voliveafrica.com/noura_services/services/change_password?cust_id=81&newpass=654321&confirmpass=654321
    
    if ((_cnformPasswordTF.text.length>0 )) {
        
    
    if( [_cnformPasswordTF.text isEqualToString:_createPasswordTF.text]){
    
    NSString *url  = [NSString stringWithFormat:@"%@change_password?cust_id=%@&newpass=%@&confirmpass=%@",base_URL,[[NSUserDefaults standardUserDefaults] objectForKey:@"customerId"],_createPasswordTF.text,_cnformPasswordTF.text];
    
    [obj GETList:url completion:^(NSDictionary *dict, NSError *err) {
        NSLog(@"change_password %@",dict);
        NSString *status = [NSString stringWithFormat:@"%@",[dict objectForKey:@"status"]];
        
        if ([status isEqualToString:@"1"])
        {
                            
                       
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //[SVProgressHUD dismiss];
                //                        [[SharedClass sharedInstance]showAlertWithTitle:@"Alert!" withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
                
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[ SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]  message:[[ SharedClass sharedInstance]languageSelectedStringForKey:@"Password changed successfully"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[ SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     [self.navigationController popViewControllerAnimated:YES];
                                                                     
                                                                 }];
                [alert addAction:okButton];
                [self presentViewController:alert animated:YES completion:nil];
                
                
            });
            
        }
        
    }];
    }else{
    _createPasswordTF.text = @"";
    _cnformPasswordTF.text = @"";
      [obj alertforMessage:self :@"cancel@1x" :@"Passwords mismatch,please enter once again" :[[ SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]];
    }
    
    }else{
    [obj alertforMessage:self :@"cancel@1x" :[[ SharedClass sharedInstance]languageSelectedStringForKey:@"Please enter password"] :[[ SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]];
    
    }
        
}
//- (IBAction)whenSubmitButtonCliked:(id)sender {
//    
//    if(![_change_Password.text isEqualToString:_confirm_Password.text])
//    {
//        UIAlertView * alert =[[UIAlertView alloc]initWithTitle:[[SharedClass sharedInstance ]languageSelectedStringForKey:@"Warning"] message:[[SharedClass sharedInstance ]languageSelectedStringForKey:@"New password and Confirm password are not Correct"] delegate:self cancelButtonTitle:[[SharedClass sharedInstance ]languageSelectedStringForKey:@"OK"] otherButtonTitles: nil];
//        [alert show];
//        
//    }
//    else
//    {
//        NSString *oldpassword = _oldPasswordTF.text;
//        NSString *cnfrmPasswrd = _change_Password.text;
//        profileID = [[NSUserDefaults standardUserDefaults]objectForKey:@"profileID"];
//        self.webServiceData_reset = [[WebServiceGetData alloc]init];
//        self.webServiceData_reset.delegate = self;
//        NSString *resetUrl = [NSString stringWithFormat:@"%@%@?user_id=%@&password=%@&new_passwprd=%@",COMMON_URL,RESET_URL,profileID,oldpassword,cnfrmPasswrd];
//        [self.webServiceData_reset getDataFromGetService:resetUrl withTag:16];
//        [self.scrollView setContentOffset:CGPointZero animated:YES];
//        
//    }
//}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    CGPoint scrollPoint=CGPointMake(0, textField.frame.origin.y-80);
    [self.scrollView setContentOffset:scrollPoint animated:YES];
}
- (void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.scrollView setContentOffset:CGPointZero animated:YES];
}
- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    //[self dismissKeyboard];
    
    return YES;
}

- (IBAction)backBtnClicked:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end

