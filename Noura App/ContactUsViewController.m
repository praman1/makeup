//
//  ContactUsViewController.m
//  Noura App
//
//  Created by volive solutions on 15/11/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "ContactUsViewController.h"
#import "SWRevealViewController.h"
#import "SharedClass.h"
#import "HeaderAppConstant.h"
@interface ContactUsViewController ()
{
    NSMutableArray *itemsArray;
    SharedClass *objForSharedClass;
    NSString *string;
    NSString *resultString;
    NSAttributedString *attrSting;
}

@end

@implementation ContactUsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self LoadCustomDesign];
    [self GetAboutUs];
    // Do any additional setup after loading the view.
}

-(void)LoadCustomDesign
{
    self.navigationItem.title = [[SharedClass sharedInstance] languageSelectedStringForKey:@"About Us"];
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        
        [_backBar setTarget: self.revealViewController];
        [_backBar setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    [revealViewController tapGestureRecognizer];

}

-(void)GetAboutUs{
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    // http://voliveafrica.com/makeup_services/services/about_us?store_id=1
    
    NSMutableDictionary * aboutUsPostDictionary = [[NSMutableDictionary alloc]init];
    
     [aboutUsPostDictionary setObject:languageStr forKey:@"store_id"];
    // [ordersPostDictionary setObject:@"1" forKey:@"store_id"];
    
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"about_us?" withPostDict:aboutUsPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        
        NSLog(@"%@",dataFromJson);
        
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        //NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
              dispatch_async(dispatch_get_main_queue(), ^{
                  
            string = [[dataDictionary objectForKey:@"data"]objectForKey:@"description"];
                  
                  NSLog(@"About Us %@",string);
                
                  //NSString *strHTML = @"S.Panchami 01.38<br>Arudra 02.01<br>V.08.54-10.39<br>D.05.02-06.52<br> <font color=red><u>Festival</u></font><br><font color=blue>Shankara Jayanthi<br></font>";
    
                  attrSting = [[NSAttributedString alloc] initWithData:[string dataUsingEncoding:NSUTF8StringEncoding]
                                                                                 options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType,
                                                                                           NSCharacterEncodingDocumentAttribute:@(NSUTF8StringEncoding)}
                                                                      documentAttributes:nil
                                                                                   error:nil];
                  
                  
                  resultString = [attrSting string];
                 // _aboutTextView.text = resultString;
                  
                  [self service];
                
            });
            
            
        }
    }];
    
    
}

-(void)service
{
    NSString *css = [NSString stringWithFormat:
                     @"<html><head><style>body { background-color: transparent; text-align: %@; font-size: %ipx; color: black;} a { color: #172983; } </style></head><body>",
                     @"left",
                     16];
    NSMutableString *desc = [NSMutableString stringWithFormat:@"%@%@%@",
                             css,
                             resultString,
                             @"</body></html>"];
    
    
    [_AboutWebView loadHTMLString:desc baseURL:nil];
}

@end
