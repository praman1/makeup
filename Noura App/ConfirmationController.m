//
//  ConfirmationController.m
//  Noura App
//
//  Created by Mohammad Apsar on 8/31/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "ConfirmationController.h"
#import "ConfirmationCell.h"
#import "CartController.h"
#import "SharedClass.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ThankYouOrderViewController.h"
@interface ConfirmationController ()

{
    NSString * addressString;
    NSMutableArray *productsArrayCount;
    NSMutableArray *productImagesArray;
    NSMutableArray *productNamesArray;
    NSMutableArray *quantityArray;
    NSMutableArray *priceArray;
    
    NSMutableArray *productTotalsCountArray;
    NSMutableArray *titleArray;
    NSMutableArray *amountArray;
    NSString *orderId;
}

@end

@implementation ConfirmationController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadViewWithCustomDesign];
   
}

-(void)loadViewWithCustomDesign {
    
    _placeBtn.layer.masksToBounds = false;
    _placeBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _placeBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _placeBtn.layer.shadowRadius = 3;
    _placeBtn.layer.shadowOpacity = 0.5;
    
    _shippingToLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Shipping To"];
    _yourOrderLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"YOUR ORDERS"];
    _paymentSummeryLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Payment Summary"];
    
//    _subTotalLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Sub Total"];
//    _shippingLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Shipping & Handling(Flat Rate - Fixed)"];
//    _grandTotalLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Grand Total"];
    
    [_placeBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"PLACE ORDER"] forState:UIControlStateNormal];
    
    productsArrayCount = [NSMutableArray new];
    productImagesArray = [NSMutableArray new];
    productNamesArray = [NSMutableArray new];
    quantityArray = [NSMutableArray new];
    priceArray = [NSMutableArray new];
    
    productTotalsCountArray = [NSMutableArray new];
    titleArray = [NSMutableArray new];
    amountArray = [NSMutableArray new];
    
    addressString=[[NSUserDefaults standardUserDefaults]objectForKey:@"addressString"];
    
    NSLog(@"MY ADFDREESS LABLE %@",addressString);
    
    _addressLbl.text=addressString;
    
    [self placeOrderServiceCall];
}


-(void)placeOrderServiceCall
{
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
//   http://voliveafrica.com/noura_services/services/get_orders_totals?cart_id=246&cust_id=81&store_id=1
    
    NSMutableDictionary *cartProductPostDictionary = [[NSMutableDictionary alloc]init];
    
    [cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"cartId"] forKey:@"cart_id"];
    
    [cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"customerId"] forKey:@"cust_id"];
    
    [cartProductPostDictionary setObject:languageStr forKey:@"store_id"];
    //[cartProductPostDictionary setObject:@"1" forKey:@"store_id"];
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"get_orders_totals?" withPostDict:cartProductPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
       // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
            });
            //NSLog(@"placeOrderServiceCall printing *****");
            dispatch_async(dispatch_get_main_queue(), ^{
                
                productsArrayCount=[dataDictionary objectForKey:@"orders"];
                productTotalsCountArray=[dataDictionary objectForKey:@"totals"];
               // categoriesImagesArrayCount=[dataDictionary objectForKey:@"image"];
                if (productsArrayCount.count>0) {
                    for (int i=0; i<productsArrayCount.count; i++) {
                        [productNamesArray addObject:[[[dataDictionary objectForKey:@"orders"]objectAtIndex:i]objectForKey:@"name"]];
                        
                        [productImagesArray addObject:[[[dataDictionary objectForKey:@"orders"]objectAtIndex:i]objectForKey:@"image"]];
                        
                        [quantityArray addObject:[[[dataDictionary objectForKey:@"orders"]objectAtIndex:i]objectForKey:@"quantity"]];
                        
                        [priceArray addObject:[[[dataDictionary objectForKey:@"orders"]objectAtIndex:i]objectForKey:@"total_price"]];
                        
                        //[[NSUserDefaults standardUserDefaults]setObject:categoriesIdArray forKey:@"catIdArray"];
                        NSLog(@"%@",productNamesArray);
                        NSLog(@"%@",productImagesArray);
                        //NSLog(@"%@",categoriesIdArray);
                        [_orderDetailsTableView reloadData];
                      }
                }
                
        if (productTotalsCountArray.count>0)
                {
                    for (int i=0; i<productTotalsCountArray.count; i++) {
                        [titleArray addObject:[[[dataDictionary objectForKey:@"totals"]objectAtIndex:i]objectForKey:@"title"]];
                        [amountArray addObject:[NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"totals"]objectAtIndex:i]objectForKey:@"amount"]]];
                        
                       }
                    if (titleArray.count == 2) {
                        _subTotalLabel.text = [titleArray objectAtIndex:0];
                        _grandTotalLabel.text = [titleArray objectAtIndex:1];
                        _orderAmountLabel.text = [amountArray objectAtIndex:0];
                        _grandTotalAmountLabel.text = [amountArray objectAtIndex:1];
                    }
                    else
                    {
                        _subTotalLabel.text = [titleArray objectAtIndex:0];
                        _shippingLabel.text = [titleArray objectAtIndex:1];
                        _grandTotalLabel.text = [titleArray objectAtIndex:2];
                        
                        _orderAmountLabel.text = [amountArray objectAtIndex:0];
                        _shippingChargeLabel.text = [amountArray objectAtIndex:1];
                        _grandTotalAmountLabel.text = [amountArray objectAtIndex:2];

                    }
                   
                }
            
            });

        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                // [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
}


-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return productNamesArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    ConfirmationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"confirmation"];
    [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[productImagesArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@""]];
    cell.productNameLabel.text = [productNamesArray objectAtIndex:indexPath.row];
    cell.productPriceLabel.text = [NSString stringWithFormat:@"%@",[priceArray objectAtIndex:indexPath.row]];
    cell.quantityLabel.text = [NSString stringWithFormat:@"%@",[quantityArray objectAtIndex:indexPath.row]];
    return cell;
}


- (IBAction)placeOrderBtn:(id)sender {
    
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:@"Loading..."];
    //[self confirmationBtnClicked];
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
   // http://voliveafrica.com/noura_services/services/place_order?cart_id=246&cust_id=81&store_id=1
    
    NSMutableDictionary *cartProductPostDictionary = [[NSMutableDictionary alloc]init];
    
    [cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"cartId"] forKey:@"cart_id"];
    [cartProductPostDictionary setObject:languageStr forKey:@"store_id"];
    [cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"customerId"] forKey:@"cust_id"];
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"place_order?" withPostDict:cartProductPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        //NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"success"]];
        
        NSLog(@"%@",dataFromJson);
        orderId = [dataFromJson valueForKey:@"data"];
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {dispatch_async(dispatch_get_main_queue(), ^{
            
            [SVProgressHUD dismiss];
            
        });
            NSLog(@"my data is printing *****");
            // cust_Id=[[dataDictionary objectForKey:@"address"]objectForKey:@"customer_id"];
            
            
            // [[NSUserDefaults standardUserDefaults] setObject:cust_Id forKey:@"cust_Id"];
            [self performSegueWithIdentifier:@"placeOrderPush" sender:self];
            
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                 [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
 
}
-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    if ([segue.identifier isEqualToString:@"placeOrderPush"]) {
        ThankYouOrderViewController *obj = [[ThankYouOrderViewController alloc] init];
        obj = [segue destinationViewController];
        obj.orderID = orderId;
    }
}
@end
