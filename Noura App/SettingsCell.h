//
//  SettingsCell.h
//  
//
//  Created by Mohammad Apsar on 7/8/17.
//
//

#import <UIKit/UIKit.h>

@interface SettingsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *labelText;

@property (weak, nonatomic) IBOutlet UILabel *selectedLbl;

@end
