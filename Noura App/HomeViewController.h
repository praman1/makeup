//
//  HomeViewController.h
//  Noura App
//
//  Created by volive solutions on 23/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
//#import <QuartzCore/QuartzCore.h>
#import <SVProgressHUD/SVProgressHUD.h>
@interface HomeViewController : UIViewController<UIScrollViewDelegate,UICollectionViewDelegate,UICollectionViewDataSource>
@property (weak, nonatomic) IBOutlet UIBarButtonItem *slideMenuBtn;
@property (weak, nonatomic) IBOutlet UIScrollView *bannerScrollview;
@property (weak, nonatomic) IBOutlet UIImageView *adImageView1;
@property (weak, nonatomic) IBOutlet UIImageView *adImageView2;
//@property NSMutableArray *bannerImagesArray;
@property UIImageView *bannerImageview;
@property (weak, nonatomic) IBOutlet UIPageControl *pageControl;
@property (weak, nonatomic) IBOutlet UICollectionView *categoriesCollectionview;
@property (weak, nonatomic) IBOutlet UICollectionView *productsCollectionview;
- (IBAction)viewAllButton:(id)sender;
@property NSMutableArray *categoriesImagesArray,*categoriesNamesArray, *productsImagesArray, *addsImagesArray,*bannerImagesArray,*productNamesArray;
@property (weak, nonatomic) IBOutlet UILabel *categoriesLabel;
@property (weak, nonatomic) IBOutlet UILabel *mostViewedLabel;
@property (weak, nonatomic) IBOutlet UIButton *viewAllBtn;
@property NSMutableArray *clothsList;

@property (weak, nonatomic) IBOutlet UITextField *search_TF;
- (IBAction)search_BTN:(id)sender;
@property (weak, nonatomic) IBOutlet UIButton *searchBtn_Outlet;

@property NSString * cartBadgeString;

@end
