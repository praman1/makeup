//
//  NewAddressViewController.m
//  Noura App
//
//  Created by volive solutions on 21/09/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "NewAddressViewController.h"
#import "SharedClass.h"
#import "SWRevealViewController.h"
#import "LineTextField.h"

@interface NewAddressViewController ()<UIPickerViewDelegate,UIPickerViewDataSource,UITextFieldDelegate>

{
    BOOL isChecked;
    NSString * status;
    NSDictionary *statusDict;
    UIPickerView *countryPickerView;
    NSMutableArray * countryArr;
    UIToolbar *toolbar1;
    NSString *  selected_Type;
    NSString * defaultString;
    SharedClass *obj;
}

@end

@implementation NewAddressViewController

- (void)viewDidLoad {
  
    [super viewDidLoad];
    
    [self loadViewWithCustomDesign];
    
    
}

-(void)loadViewWithCustomDesign
{
    self.navigationItem.title = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Add New Address"];
    _addNewAddressBtn.layer.masksToBounds = false;
    _addNewAddressBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _addNewAddressBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _addNewAddressBtn.layer.shadowRadius = 3;
    _addNewAddressBtn.layer.shadowOpacity = 0.5;
    
    int selectedLanguage = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    
    if(selectedLanguage == 2){
        
        _firstNameTF.textAlignment = NSTextAlignmentRight;
        _lastNameTF.textAlignment = NSTextAlignmentRight;
        _phoneNumberTF.textAlignment = NSTextAlignmentRight;
        _companyTF.textAlignment = NSTextAlignmentRight;
        _address1TF.textAlignment = NSTextAlignmentRight;
        _address2TF.textAlignment = NSTextAlignmentRight;
        _cityTF.textAlignment = NSTextAlignmentRight;
        _pincodeTF.textAlignment = NSTextAlignmentRight;
        _stateTF.textAlignment = NSTextAlignmentRight;
        _countryTF.textAlignment = NSTextAlignmentRight;
        countryArr=[[NSMutableArray alloc]initWithObjects:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Saudi Arabia"], nil];
        
    } else  {
        
        _firstNameTF.textAlignment = NSTextAlignmentLeft;
        _lastNameTF.textAlignment = NSTextAlignmentLeft;
        _phoneNumberTF.textAlignment = NSTextAlignmentLeft;
        _companyTF.textAlignment = NSTextAlignmentLeft;
        _address1TF.textAlignment = NSTextAlignmentLeft;
        _address2TF.textAlignment = NSTextAlignmentLeft;
        _cityTF.textAlignment = NSTextAlignmentLeft;
        _pincodeTF.textAlignment = NSTextAlignmentLeft;
        _stateTF.textAlignment = NSTextAlignmentLeft;
        _countryTF.textAlignment = NSTextAlignmentLeft;
        countryArr=[[NSMutableArray alloc]initWithObjects:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Saudi Arabia"], nil];
    }

    
    
    _addAddressScrollView.delegate = self;
    obj = [[SharedClass alloc]init];
    
    isChecked = NO;
    
   // countryArr=[[NSMutableArray alloc]initWithObjects:@"Saudi Arabia", nil];
    
    countryPickerView = [[UIPickerView alloc] init];
    
    countryPickerView.delegate = self;
    countryPickerView.dataSource = self;
    _countryTF.inputView = countryPickerView;
    
    toolbar1= [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, 44)];
    toolbar1.barStyle = UIBarStyleBlackOpaque;
    toolbar1.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    toolbar1.backgroundColor=[UIColor whiteColor];
    
    UIBarButtonItem *doneButton1 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Done"]  style: UIBarButtonItemStyleDone target: self action: @selector(show)];
    [doneButton1 setTintColor:[UIColor whiteColor]];
    UIBarButtonItem* flexibleSpace1= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *cancelButton1 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Cancel"]  style: UIBarButtonItemStyleDone target: self action: @selector(cancel)];
    [cancelButton1 setTintColor:[UIColor whiteColor]];
    toolbar1.items = [NSArray arrayWithObjects:cancelButton1,flexibleSpace1,doneButton1, nil];
    [_countryTF setInputAccessoryView:toolbar1];
    
    [_addNewAddressBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Add a New Address"] forState:UIControlStateNormal];
    
    _defaultShippingLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Use as my default shipping address"];
    
    LineTextField *line = [[LineTextField alloc]init];
    
    [line textfieldAsLine:_firstNameTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"First Name"] myView:self.view];
    [line textfieldAsLine:_lastNameTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Last Name"] myView:self.view];
    [line textfieldAsLine:_phoneNumberTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Mobile Number"] myView:self.view];
    [line textfieldAsLine:_companyTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Company"] myView:self.view];
    
    [line textfieldAsLine:_address1TF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"StreetAddress1"] myView:self.view];
    [line textfieldAsLine:_address2TF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"StreetAddress2"] myView:self.view];
    [line textfieldAsLine:_cityTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"City"] myView:self.view];
    [line textfieldAsLine:_pincodeTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Pin Code"] myView:self.view];
    [line textfieldAsLine:_stateTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"State"] myView:self.view];
    [line textfieldAsLine:_countryTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Country"] myView:self.view];
    
    


}

- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _firstNameTF) {
        
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-40) animated:YES];
        
        
        
    }else if (textField == _lastNameTF) {
        
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-40) animated:YES];
        
    }else if (textField == _phoneNumberTF) {
        
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _companyTF) {
        
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _address1TF) {
        
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _address2TF) {
        
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _cityTF) {
        
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _pincodeTF) {
        
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _countryTF) {
        
        [_addAddressScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }

}



-(void)show
{
//    countryPickerView.hidden = YES;
//    toolbar1.hidden = YES;
    if(selected_Type.length == 0)
    {
        _countryTF.text = [countryArr objectAtIndex:0];
        
    }  else
    {
       _countryTF.text = selected_Type;
    }

    NSLog(@"myTextValue *** %@",_countryTF.text);
    [_addAddressScrollView setContentOffset:CGPointZero animated:YES];
    
    [_countryTF resignFirstResponder];
    
}
-(void)cancel
{
     [_countryTF resignFirstResponder];
    
}
//}

- (IBAction)countryBtn:(id)sender {
}

- (IBAction)defaultShippingBtn:(id)sender {

    NSLog(@"defalt shipping btton pressed");
    
     isChecked=!isChecked;
    if (isChecked == YES) {
        _defaultShippingCheckImage.image=[UIImage imageNamed:@"check"];
        defaultString = [NSString stringWithFormat:@"True"];
        
    }else
    {
        _defaultShippingCheckImage.image=[UIImage imageNamed:@"checkbox"];
        defaultString = [NSString stringWithFormat:@"False"];
    }
        
    
    
    
    
}

- (IBAction)addNewAddrBtn:(id)sender {
    
    //http://voliveafrica.com/noura_services/services/add_address?customer_id=39&email=raj@gmail.com&first_name=joh,&company=volive&last_name=J&street1=1-23-22&street2=model%20colony&city=Hyderabad&country_id=sa&postcode=500045&telephone=9797979797&is_default_shipping=FALSE
    
    if ((_firstNameTF.text.length>0 && _lastNameTF.text.length>0 && _phoneNumberTF.text.length>0 && _companyTF.text.length>0 && _address1TF.text.length>0 && _address2TF.text.length>0 && _cityTF.text.length>0 && _pincodeTF.text.length>0 && _countryTF.text.length>0 && isChecked == YES)) {
    
    NSString *url = @"http://voliveafrica.com/makeup_services/services/add_address";
    
    NSMutableDictionary * addAddressPostDictionary = [[NSMutableDictionary alloc]init];
    
    [addAddressPostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"customerId"] forKey:@"customer_id"];
    [addAddressPostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"emailId"] forKey:@"email"];
                [addAddressPostDictionary setObject:_firstNameTF.text forKey:@"first_name"];
                [addAddressPostDictionary setObject:_lastNameTF.text forKey:@"last_name"];
                [addAddressPostDictionary setObject:_phoneNumberTF.text forKey:@"telephone"];
                [addAddressPostDictionary setObject:_companyTF.text forKey:@"company"];
                [addAddressPostDictionary setObject:_address1TF.text forKey:@"street1"];
                [addAddressPostDictionary setObject:_address2TF.text forKey:@"street2"];
                [addAddressPostDictionary setObject:_cityTF.text forKey:@"city"];
                [addAddressPostDictionary setObject:_pincodeTF.text forKey:@"postcode"];
                [addAddressPostDictionary setObject:_countryTF.text forKey:@"country_id"];
                [addAddressPostDictionary setObject:defaultString forKey:@"is_default_shipping"];
    
    
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in addAddressPostDictionary) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [addAddressPostDictionary objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
    }
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSLog(@"The request is %@",request);
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *  _Nullable data, NSURLResponse *_Nullable response, NSError * _Nullable error) {
        
        statusDict = [[NSDictionary alloc]init];
        statusDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"Images from server %@", statusDict);
         status = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"status"]];
        dispatch_async(dispatch_get_main_queue(), ^{
            if ([status isEqualToString:@"1"])
            {
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
                                            
                                                                               message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Address successfully Inserted"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     
                                                                     [self.navigationController popViewControllerAnimated:YES];
                                                                     
                                                                 }];
                [alert addAction:okButton];
                [self presentViewController:alert animated:YES completion:nil];
                
            }
            //            else
            //            {
            //                [obj alertforMessage:self :@"cancel@1x" :@"Please Enter details" :@"Ok"];
            //                
            //            }
        });
        
    }]
     
     resume];
    }else
    {
        [obj alertforMessage:self :@"cancel@1x" :[[SharedClass sharedInstance]languageSelectedStringForKey:@"Please Enter details"] :[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]];
        
    }

}

- (IBAction)backBarBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if (pickerView == countryPickerView) {
        return 1;
    }
    
    return 0;
}

// #4
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == countryPickerView) {
        return [countryArr count];
    }
    
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == countryPickerView) {
        selected_Type = countryArr[row];
    }
    
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{

    if (pickerView == countryPickerView) {
        return countryArr[row];
    }
    
    return nil;
   
}
//- (CGFloat)pickerView:(UIPickerView *)pickerView widthForComponent:(NSInteger)component {
//    int sectionWidth = 300;
//    return sectionWidth;
//}



@end
