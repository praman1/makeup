//
//  SlideMenuViewController.m
//  Noura App
//
//  Created by volive solutions on 23/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "SlideMenuViewController.h"
#import "SlideMenuTableViewCell.h"
#import "HomeViewController.h"
#import "ProfileViewController.h"
#import "LoginViewController.h"
#import "TabBarController.h"
#import "AppDelegate.h"
#import "LoginViewController.h"
#import "SettingsController.h"
#import "ContactUsViewController.h"
#import "SharedClass.h"
#import "SWRevealViewController.h"
@interface SlideMenuViewController ()
{
    SlideMenuTableViewCell *menuItemCell;
    AppDelegate * menuAppDelegate;
    NSString *type;
    NSString *titleStr;
    NSString *  cartBadgeStr;
    HomeViewController * home;
    
    
}

@end

@implementation SlideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    menuAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    //[self cartBadgeValueServiceCall];
   // [self loadViewWithCustomDesign];
    // Do any additional setup after loading the view.
    
    cartBadgeStr=[[NSUserDefaults standardUserDefaults]stringForKey:@"totalBadge"];
    
}

-(void)viewWillAppear:(BOOL)animated
{
    int selectedLanguage = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    
    if(selectedLanguage == 2){
        
        _menuNamesArray = [[NSMutableArray alloc]initWithObjects:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Home"],[[SharedClass sharedInstance]languageSelectedStringForKey:@"My Favourites"],[[SharedClass sharedInstance]languageSelectedStringForKey:@"My Account"],[[SharedClass sharedInstance]languageSelectedStringForKey:@"About Us"],[[SharedClass sharedInstance]languageSelectedStringForKey:@"Settings"],[[SharedClass sharedInstance]languageSelectedStringForKey:@"Logout"], nil];
        
    } else  {
        
        _menuNamesArray = [[NSMutableArray alloc]initWithObjects:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Home"],[[SharedClass sharedInstance]languageSelectedStringForKey:@"My Favourites"],[[SharedClass sharedInstance]languageSelectedStringForKey:@"My Account"],[[SharedClass sharedInstance]languageSelectedStringForKey:@"About Us"],[[SharedClass sharedInstance]languageSelectedStringForKey:@"Settings"],[[SharedClass sharedInstance]languageSelectedStringForKey:@"Logout"], nil];
        
    }
    [_slideMenuTableview reloadData];

    [self loadViewWithCustomDesign];
//    [self.revealViewController.frontViewController.view setUserInteractionEnabled:NO];
//    [self.revealViewController.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
}

-(void)loadViewWithCustomDesign {
    
    _nameView.layer.borderColor = [[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]CGColor];
    _nameView.layer.borderWidth = 1.2f;
    _nameLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"name"];
    _emailLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"emailId"];
    _nameView.layer.cornerRadius = _nameView.frame.size.width/2;
   // _nameLetterLabel.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"name"];
    
    titleStr = _nameLabel.text;
    
    NSMutableString * firstCharacters = [NSMutableString string];
    NSArray * words = [titleStr componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    for (NSString * word in words) {
        if ([word length] > 1) {
            NSString * firstLetter = [word substringToIndex:1];
            [firstCharacters appendString:[firstLetter uppercaseString]];
            
            _nameLetterLabel.text=firstCharacters;
            //[[NSUserDefaults standardUserDefaults]setObject:_nameLetterLabel.text forKey:@"letter"];
        }
    }

    [self.revealViewController.frontViewController.view setUserInteractionEnabled:NO];
    [self.revealViewController.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];

    
    
    
    _slideMenuTableview.delegate = self;
    _slideMenuTableview.dataSource = self;
    
    _homeLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Home"];
    
    _menuImagesArray = [[NSMutableArray alloc]initWithObjects:@"menu home",@"NewWish-List",@"menu my account",@"contact",@"menu settings",@"menu logout", nil];
   
    
    
    
}


- (void)viewWillDisappear:(BOOL)animated {
    
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES]; // it shows
    [self.revealViewController.frontViewController.view setUserInteractionEnabled:YES];
    
    
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
//- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
//    // Get the new view controller using [segue destinationViewController].
//    // Pass the selected object to the new view controller.
//    if ([segue.identifier isEqualToString:@"pushToSettings"]) {
//        SettingsController *obj = [[SettingsController alloc] init];
//        obj = [segue destinationViewController];
//        obj.type = type;
//    }
//   
//    
//}

//-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
//{
//    return 0;
//}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_menuNamesArray count];
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    menuItemCell = [tableView dequeueReusableCellWithIdentifier:@"menuItemCell"];
    if(menuItemCell == nil)
    {
    menuItemCell = [[SlideMenuTableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"menuItemCell"];
    }
    menuItemCell.menuImageView.image =[UIImage imageNamed:[_menuImagesArray objectAtIndex:indexPath.row]];
    menuItemCell.menuNameLabel.text = [_menuNamesArray objectAtIndex:indexPath.row];
    
    return menuItemCell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
     menuItemCell = [tableView cellForRowAtIndexPath:indexPath];
    switch (indexPath.row) {
        case 0:{
           
            [self performSegueWithIdentifier:@"pushToHome" sender:self];
             menuAppDelegate.selectTabItem = @"0";
           [[[[[self tabBarController] tabBar] items] objectAtIndex:1] setBadgeValue:cartBadgeStr];
            
            
            //home.cartBadgeString=cartBadgeStr;
            
            break;
         }
        case 1:{
            
           // type = @"pushToWish";
            
            //[[NSUserDefaults standardUserDefaults]setValue:@"pushToWish" forKey:@"about"];
            [self performSegueWithIdentifier:@"pushToWish" sender:self];
            menuAppDelegate.selectTabItem = @"3";
            [[[[[self tabBarController] tabBar] items] objectAtIndex:1] setBadgeValue:cartBadgeStr];
            
            break;
        }case 2:{
            
            type = @"pushToProfile";
            
            [[NSUserDefaults standardUserDefaults]setValue:@"pushToProfile" forKey:@"about"];
            [self performSegueWithIdentifier:@"pushToProfile" sender:self];
            [[[[[self tabBarController] tabBar] items] objectAtIndex:1] setBadgeValue:cartBadgeStr];
            break;
        }case 3:{
            
            [self performSegueWithIdentifier:@"pushToContact" sender:self];
          [[[[[self tabBarController] tabBar] items] objectAtIndex:1] setBadgeValue:cartBadgeStr];
            break;
        }
        case 4:{
            
            type = @"settings";
            
            [[NSUserDefaults standardUserDefaults]setValue:@"settings" forKey:@"about"];
            [self performSegueWithIdentifier:@"pushToSettings" sender:self];
            [[[[[self tabBarController] tabBar] items] objectAtIndex:1] setBadgeValue:cartBadgeStr];
            break;
        }
        
        case 5:{
            UIAlertController * alert = [UIAlertController
                                         alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Logout"]
                                         message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Are you sure to Logout?"]
                                         preferredStyle:UIAlertControllerStyleAlert];
            
            //Add Buttons
            
            UIAlertAction* yesButton = [UIAlertAction
                                        actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"YES"]
                                        style:UIAlertActionStyleDestructive
                                        handler:^(UIAlertAction * action) {
                                            //Handle your yes please button action here
                                    
                                            
                                            UIViewController * gotoMainNav = [self.storyboard instantiateViewControllerWithIdentifier:@"loginNavVC"];
                                            [[UIApplication sharedApplication] delegate].window.rootViewController = gotoMainNav;
                                            [[[UIApplication sharedApplication] delegate].window makeKeyAndVisible];
                                            
                                           // [self clearAllData];
                                        }];
            
            UIAlertAction* noButton = [UIAlertAction
                                       actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Cancel"]
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction * action) {
                                           //Handle no, thanks button
                                       }];
            
            
            
            //Add your buttons to alert controller
            [alert addAction:noButton];
            [alert addAction:yesButton];
            
            
            [self presentViewController:alert animated:YES completion:nil];
            break;
        }
        default:
            break;
}

}



- (IBAction)homeButton:(id)sender {
    menuAppDelegate.selectTabItem = @"0";
    HomeViewController * home1 = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeViewController"];
    [self.navigationController pushViewController:home1 animated:YES];
    
}
@end
