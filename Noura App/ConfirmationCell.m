//
//  ConfirmationCell.m
//  Noura App
//
//  Created by Mohammad Apsar on 8/31/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "ConfirmationCell.h"
#import "SharedClass.h"
@implementation ConfirmationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    _quantityNameLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Quantity :"];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
