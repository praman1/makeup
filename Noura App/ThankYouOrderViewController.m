//
//  ThankYouOrderViewController.m
//  Noura App
//
//  Created by volive solutions on 22/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "ThankYouOrderViewController.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "SharedClass.h"
@interface ThankYouOrderViewController ()
{
    AppDelegate* appDelegate;
}

@end

@implementation ThankYouOrderViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadViewWithCustomDesign];
    
    // Do any additional setup after loading the view.
}

-(void)loadViewWithCustomDesign {
    
    _continueShoppingBtn.layer.masksToBounds = false;
    _continueShoppingBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _continueShoppingBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _continueShoppingBtn.layer.shadowRadius = 3;
    _continueShoppingBtn.layer.shadowOpacity = 0.5;
    
    
    
    appDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    
    
    NSString *str = [NSString stringWithFormat:@"%@ : %@",[[SharedClass sharedInstance]languageSelectedStringForKey:@"Your Order Number :"],_orderID];
    _orderNumberLabel.text = str;
    
    NSLog(@"My Order Number is %@",_orderNumberLabel.text);
    [_continueShoppingBtn setTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"CONTINUE SHOPPING"] forState:UIControlStateNormal];
    _thankYouLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"THANK YOU"];

}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)continueShoppingBtn:(id)sender {
    SWRevealViewController*reveal=[self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self presentViewController:reveal animated:YES completion:nil];
}
@end
