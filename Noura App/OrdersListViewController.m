//
//  OrdersListViewController.m
//  Noura App
//
//  Created by volive solutions on 22/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "OrdersListViewController.h"
#import "OrdersListTableViewCell.h"
#import "SWRevealViewController.h"
#import "OrderDetailsViewController.h"
#import "SharedClass.h"
#import "SVProgressHUD.h"
@interface OrdersListViewController ()
{
    OrdersListTableViewCell *orderListCell;
    NSMutableArray *ordersCountArray;
    SharedClass *objForSharedClass;

}

@end

@implementation OrdersListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadViewWithCustomDesign];
    
    
    
  }

-(void)loadViewWithCustomDesign {
    
    self.navigationItem.title = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Orders"];

   // [self.tabBarItem setTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Orders"]];
    objForSharedClass = [[SharedClass alloc] init];
    _ordersListTableview.delegate = self;
    _ordersListTableview.dataSource = self;
    
    _ordersDateArray = [[NSMutableArray alloc]init];
    _ordersIdArray = [[NSMutableArray alloc]init];
    _orderStatusArray = [[NSMutableArray alloc]init];
    
    [[[[self.tabBarController tabBar] items] objectAtIndex:0] setTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Home"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:1] setTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Cart"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:2] setTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Orders"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:3] setTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"My Favourites"]];

    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        
        [_sideMenu setTarget: self.revealViewController];
        [_sideMenu setAction: @selector( revealToggle: )];
    }
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    [self ordersListServiceCall];
 
    
    
    
    
}


-(void)ordersListServiceCall
{
//    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:206.0/255.0 green:170.0/255.0 blue:109.0/255.0 alpha:1.0]];
//    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
//    [SVProgressHUD showWithStatus:@"Loading..."];
  // [objForSharedClass alertforLoading:self];

    
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
   // http://voliveafrica.com/noura_services/services/user_orders?user_id=39&store_id=1
    
    NSMutableDictionary * ordersPostDictionary = [[NSMutableDictionary alloc]init];
    [ordersPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@"user_id"];
    [ordersPostDictionary setObject:languageStr forKey:@"store_id"];
   // [ordersPostDictionary setObject:@"1" forKey:@"store_id"];
    
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"user_orders?" withPostDict:ordersPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        
        NSLog(@"%@",dataFromJson);
        
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        //NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
               // [SVProgressHUD dismiss];

                //[objForSharedClass.hud hideAnimated:YES];
            });
            
            dispatch_async(dispatch_get_main_queue(), ^{
                ordersCountArray = [dataDictionary objectForKey:@"data"];
                if(ordersCountArray.count>0)
                {
                        for (int i=0; i<ordersCountArray.count; i++) {
                        
                        [_ordersDateArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"created_at"]];
                        
                        [_ordersIdArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"order_id"]];
                        
                        [_orderStatusArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"status"]];
                        
                        [[NSUserDefaults standardUserDefaults]setObject:_ordersIdArray forKey:@"orderIdArray"];
                            
                            
                        NSLog(@"Mydata %@",_ordersDateArray);
                        NSLog(@"Mydata %@",_ordersIdArray);
                        NSLog(@"Mydata %@",_orderStatusArray);
                    }
                }
                [_ordersListTableview reloadData];
                
                
            });
            
        
        }else
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(ordersCountArray.count>0){
                    
                    //[SVProgressHUD dismiss];
                    
                }else{
                    [objForSharedClass alertforMessage:self :@"cancel@1x" :[[SharedClass sharedInstance]languageSelectedStringForKey:@"No products in Orders"] :[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]];
                }
                
                //[objForSharedClass.hud hideAnimated:YES];
            });
        }
        
    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _ordersIdArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    orderListCell  = [_ordersListTableview dequeueReusableCellWithIdentifier:@"ordersListCell" forIndexPath:indexPath];
    orderListCell.orderDateLabel.text = [_ordersDateArray objectAtIndex:indexPath.row];
    orderListCell.orderIdLabel.text = [_ordersIdArray objectAtIndex:indexPath.row];
    orderListCell.orderStatusLabel.text = [_orderStatusArray objectAtIndex:indexPath.row];
    
    return  orderListCell;
}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(tableView == _ordersListTableview)
    {
        OrderDetailsViewController *ordersList = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderDetailsViewController"];
        [ordersList orderDetailsServiceCall:[_ordersIdArray objectAtIndex:indexPath.row]];
        ordersList.ordersIdArray = _ordersIdArray;
//        ordersList.clothNameArr = self.clothsList;
//        ordersList.clothIDsArr = clothsIdArray;
        
        
        
        [self.navigationController pushViewController:ordersList animated:YES];
        //[self performSegueWithIdentifier:@"productsPush" sender:self];
    }
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
