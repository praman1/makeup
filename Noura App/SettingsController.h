//
//  SettingsController.h
//  Together
//
//  Created by Mohammad Apsar on 7/8/17.
//  Copyright © 2017 murali krishna. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SettingsController : UIViewController<UITableViewDataSource,UITableViewDelegate>

@property (strong,nonatomic) NSMutableArray *items_array;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *sideMenu;
@property (weak, nonatomic) IBOutlet UITableView *setting_table;

@property (weak,nonatomic) NSString *type;


@end
