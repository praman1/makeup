//
//  WishListViewController.m
//  Noura App
//
//  Created by volive solutions on 22/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "WishListViewController.h"
#import "WishListTableViewCell.h"
#import "SWRevealViewController.h"
#import "AppDelegate.h"
#import "SharedClass.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "ProductsViewController.h"
#import "SVProgressHUD.h"
#import "MBProgressHUD.h"



@interface WishListViewController ()
{
    WishListTableViewCell *wishListCell;
    AppDelegate *appdelegate;
    NSMutableArray *wishListArrayCount;
    NSMutableArray *idArray;
     SharedClass *objForSharedClass;
    MBProgressHUD *hud;
    NSMutableDictionary * mutableDictionaryOfTextValues;
   
    NSMutableArray *stockArray,*instockArray;
    NSString * stockStr;
    NSString * instackStr;
    NSString * cartBadgeString;
    NSString *stepperValue;
    //ANStepperView *stepper;
}

@end

@implementation WishListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self cartBadgeValueServiceCall];
    
   
        //[self addToCartClicked:self];
    // Do any additional setup after loading the view.
    
}

-(void)viewWillAppear:(BOOL)animated
{
    [self viewDidLoad];
    [self loadViewWithCustomDesign];
    
    //[objForSharedClass.hud hideAnimated:YES];
}

-(void)loadViewWithCustomDesign {

 //  [self.tabBarItem setTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"WishList"]];
    self.navigationItem.title = [[SharedClass sharedInstance] languageSelectedStringForKey:@"My Favourites"];

    objForSharedClass = [[SharedClass alloc] init];
    
    
    _wishListTableview.delegate = self;
    _wishListTableview.dataSource = self;
    wishListCell.quantityTF.delegate=self;
    
    _wishListImagesArray = [[NSMutableArray alloc]init];
    _productNamesArray = [[NSMutableArray alloc]init];
    _presentAmmountArray = [[NSMutableArray alloc]init];
    _oldAmountArray = [[NSMutableArray alloc]init];
    idArray = [NSMutableArray new];
    appdelegate =(AppDelegate *) [[UIApplication sharedApplication ]delegate];
    
    [[[[self.tabBarController tabBar] items] objectAtIndex:0] setTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Home"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:1] setTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Cart"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:2] setTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Orders"]];
    [[[[self.tabBarController tabBar] items] objectAtIndex:3] setTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"My Favourites"]];

    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        
        [_backBarBtn setTarget: self.revealViewController];
        [_backBarBtn setAction: @selector( revealToggle: )];
    }
//    wishListCell.quantityLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Quantity :"];
//    [wishListCell.addToCartBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"ADD TO CART"] forState:UIControlStateNormal];
    [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    
    [self wishListServiceCall];



}

-(void)wishListServiceCall
{
//    [SVProgressHUD setDefaultStyle:SVProgressHUDStyleCustom];
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    //[objForSharedClass alertforLoading:self];
    
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    
  //  http://voliveafrica.com/makeup_services/services/get_wishlist?customer_id=31&store_id=1
    NSMutableDictionary *wishListPostDictionary = [[NSMutableDictionary alloc]init];

    [wishListPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@"customer_id"];
    [wishListPostDictionary setObject:languageStr forKey:@"store_id"];
    
    // [wishListPostDictionary setObject:@"1" forKey:@"store_id"];
    
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"get_wishlist?" withPostDict:wishListPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        //NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            
            dispatch_async(dispatch_get_main_queue(), ^{
               // [objForSharedClass.hud hideAnimated:YES];
                //[hud hideAnimated:YES];
                [SVProgressHUD dismiss];
                
            });
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                _wishListImagesArray = [[NSMutableArray alloc]init];
                _productNamesArray = [[NSMutableArray alloc]init];
                _presentAmmountArray = [[NSMutableArray alloc]init];
                _oldAmountArray = [[NSMutableArray alloc]init];
                idArray = [NSMutableArray new];
                stockArray = [NSMutableArray new];
                instockArray = [NSMutableArray new];

                
                wishListArrayCount=[dataDictionary objectForKey:@"data"];
                
                if (wishListArrayCount.count>0) {
                    for (int i=0; i<wishListArrayCount.count; i++) {
                        [_wishListImagesArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"image"]];
                        [_productNamesArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                        [_presentAmmountArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"price"]];
                        [idArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"]];
                        [stockArray addObject:[NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"stock"]]];
                        [instockArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"instock"]];
                        NSLog(@"Stock Array Is %@",stockArray);
                        NSLog(@"InStock Array Is %@",instockArray);
                        
                        [[NSUserDefaults standardUserDefaults] setObject:idArray forKey:@"idsArray"];
                        //[productRatingArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"rating"]];
                        
                        // NSLog(@"%@",_clothsList);
                    }
                }
                [_wishListTableview reloadData];
                
                dispatch_async(dispatch_get_main_queue(), ^{
                    //[hud hideAnimated:YES afterDelay:1.0];
                    if(wishListArrayCount.count>0){
                        [_wishListTableview reloadData];
                    }else{
                        [objForSharedClass alertforMessage:self :@"cancel@1x" :[[SharedClass sharedInstance]languageSelectedStringForKey:@"No products in wishlist"] :[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]];
                    }
                });
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //[SVProgressHUD dismiss];
                
                //[objForSharedClass.hud hideAnimated:YES];
                
                [[SharedClass sharedInstance]showAlertWithTitle:@"Alert!" withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];

}

-(void)removeWishListServiceCall:(id)sender
{
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];

    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.wishListTableview];
    NSIndexPath *indexPath = [self.wishListTableview indexPathForRowAtPoint:btnPosition];
    
    //http://voliveafrica.com/noura_services/services/add_or_remove_wishlist?add_or_remove=remove&product_id=1&store_id=1&customer_id=39
    
    NSMutableDictionary *wishListPostDictionary = [[NSMutableDictionary alloc]init];
    [wishListPostDictionary setObject:@"remove" forKey:@"add_or_remove"];
    [wishListPostDictionary setObject:[idArray objectAtIndex:indexPath.row] forKey:@"product_id"];
    [wishListPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@"customer_id"];
    [wishListPostDictionary setObject:languageStr forKey:@"store_id"];
    //[wishListPostDictionary setObject:@"1" forKey:@"store_id"];
    
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"add_or_remove_wishlist?" withPostDict:wishListPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        //NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                    [SVProgressHUD dismiss];
                
            });
            
            
                [idArray removeObjectAtIndex:indexPath.row];
                [_wishListImagesArray removeObjectAtIndex:indexPath.row];
                [_productNamesArray removeObjectAtIndex:indexPath.row];
                [_presentAmmountArray removeObjectAtIndex:indexPath.row];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                NSLog(@"Product successfully added to wishlist******");
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
                                            
                                                                               message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Product successfully removed from wishlist"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     
                                                                 }];
                [alert addAction:okButton];
                [self presentViewController:alert animated:YES completion:nil];
                dispatch_async(dispatch_get_main_queue(), ^{
                [_wishListTableview reloadData];
                });
                
            });
            
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:@"Error!" withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];

    
}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return _productNamesArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    wishListCell = [tableView dequeueReusableCellWithIdentifier:@"wishListCell"];
    if(wishListCell == nil)
    {
        wishListCell = [[WishListTableViewCell alloc]initWithStyle:UITableViewCellStyleDefault reuseIdentifier:@"wishListCell"];
    }
    [wishListCell.wishListImage sd_setImageWithURL:[NSURL URLWithString:[_wishListImagesArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@""]];
    wishListCell.productNameLabel.text = [_productNamesArray objectAtIndex:indexPath.row];
    
    wishListCell.userInteractionEnabled=YES;
    
    
    wishListCell.presentAmountLabel.text = [NSString stringWithFormat:@"%@",[_presentAmmountArray objectAtIndex:indexPath.row]];
    wishListCell.deleteBtn.tag=indexPath.row;
    
    [wishListCell.deleteBtn addTarget:self action:@selector(removeWishListServiceCall:) forControlEvents:UIControlEventTouchUpInside];
   
    [wishListCell.addToCartBtn addTarget:self action:@selector(addToCartServiceCall:) forControlEvents:UIControlEventTouchUpInside];
   // [wishListCell.stepperBtn_Outlet addTarget:self action:@selector(stepperClicked:) forControlEvents:UIControlEventTouchUpInside];
     stockStr=[stockArray objectAtIndex:indexPath.row];
    instackStr=[instockArray objectAtIndex:indexPath.row];
    
    if  ([stockStr isEqualToString:@"0"] || [instackStr isEqualToString:@"0"]) {
        
        [wishListCell.addToCartBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"NOTIFY ME"] forState:UIControlStateNormal];
    }else
    {
        [wishListCell.addToCartBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"ADD TO CART"] forState:UIControlStateNormal];
    }
    
    //    wishListCell.oldAmountLabel.text = [_oldAmountArray objectAtIndex:indexPath.row];
    return wishListCell;
}
- (IBAction)addToCartClicked:(id)sender {
   
  
    
//    self.tabBarController.selectedIndex= 1;
//    NSInteger tabitem = self.tabBarController.selectedIndex;
//    [[self.tabBarController.viewControllers objectAtIndex:tabitem] popToRootViewControllerAnimated:YES];

}

//-(void)textFieldDidEndEditing:(UITextField*)aTextField
//{
//    WishListTableViewCell * cell  = (WishListTableViewCell*)aTextField.superview;
//    NSIndexPath* indexPath = [self.wishListTableview indexPathForCell:cell];
//    // Store the text, for example in an NSMutableDictionary using the indexPath as a key
//    [mutableDictionaryOfTextValues setValue:aTextField.text forKey:indexPath];
//}
//
//-(void)stepperValue
//{
//   // ANStepperView * stepper = (ANStepperView *)sender;
//    stepperValue = [NSString stringWithFormat:@"%f",stepper.value];
//    NSLog(@"Stepper Value Is %@",stepperValue);
//
//}
-(void)addToCartServiceCall:(id)sender
{
    if ([instackStr isEqualToString:@"0"] || [stockStr isEqualToString:@"0"]) {
        
        UIAlertController * alertController = [UIAlertController alertControllerWithTitle: @"Alert!"
                                                                                  message: @""
                                                                           preferredStyle:UIAlertControllerStyleAlert];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Guest";
            textField.backgroundColor = [UIColor colorWithRed:216.0f/255.0f green:216.0f/255.0f blue:216.0f/255.0f alpha:1];
            textField.textColor = [UIColor blueColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.borderStyle = UITextBorderStyleRoundedRect;
        }];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Mead@volive.me";
            textField.backgroundColor = [UIColor colorWithRed:216.0f/255.0f green:216.0f/255.0f blue:216.0f/255.0f alpha:1];
            textField.textColor = [UIColor blueColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.borderStyle = UITextBorderStyleRoundedRect;
            textField.secureTextEntry = YES;
        }];
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
            textField.placeholder = @"Phone Number";
            textField.backgroundColor = [UIColor colorWithRed:216.0f/255.0f green:216.0f/255.0f blue:216.0f/255.0f alpha:1];
            textField.textColor = [UIColor blueColor];
            textField.clearButtonMode = UITextFieldViewModeWhileEditing;
            textField.borderStyle = UITextBorderStyleRoundedRect;
            textField.secureTextEntry = YES;
        }];
        
        UIAlertAction *cancelAction=[UIAlertAction actionWithTitle:@"Notify Me" style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
            NSArray * textfields = alertController.textFields;
            UITextField * namefield = textfields[0];
            UITextField * passwordfiled = textfields[1];
            UITextField * phoneNumberField = textfields[2];
            NSLog(@"%@:%@:%@",namefield.text,passwordfiled.text,phoneNumberField.text);
            
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
            
                                                                                   message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"We will give an update when the product is available"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                       style:UIAlertActionStyleCancel
                                                                     handler:^(UIAlertAction * _Nonnull action) {
            
                                                                     }];
            [alert addAction:okButton];
                    
            [self presentViewController:alert animated:YES completion:^{ }];

            
        }];
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"Cancel"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                   }];
        
        [alertController addAction:okAction];
        [alertController addAction:cancelAction];
        [self presentViewController:alertController animated:YES completion:nil];

        
    }
    else
    {
        [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
        [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
        [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
        
        if (stepperValue>0) {
            
            
            NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
            CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.wishListTableview];
            NSIndexPath *indexPath = [self.wishListTableview indexPathForRowAtPoint:btnPosition];
            
            WishListTableViewCell * cell = [self.wishListTableview cellForRowAtIndexPath:indexPath];
            
            _quantityStr=cell.quantityTF.text;
            
            //http://voliveafrica.com/noura_services/services/add_product_to_cart?customer_id=39&product_id=2&product_qty=2&store_id=1
            NSMutableDictionary *addToCartPostDictionary = [[NSMutableDictionary alloc]init];
            
            [addToCartPostDictionary setObject:[idArray objectAtIndex:indexPath.row]forKey:@"product_id"];
            [addToCartPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@"customer_id"];
            [addToCartPostDictionary setObject:languageStr forKey:@"store_id"];
            [addToCartPostDictionary setObject:stepperValue forKey:@"product_qty"];
            //[addToCartPostDictionary setObject:@"1" forKey:@"store_id"];
            
            [[SharedClass sharedInstance]fetchResponseforParameter:@"add_product_to_cart?" withPostDict:addToCartPostDictionary andReturnWith:^(NSData *dataFromJson) {
                
                NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
                
                NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
                //NSString * wishListValue = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
                NSLog(@"%@",dataFromJson);
                NSLog(@"%@ My Data Is",dataDictionary);
                
                if ([status isEqualToString:@"1"])
                {
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [SVProgressHUD dismiss];
                        
                    });
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        NSLog(@"Product successfully added to Cart******");
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
                                                    
                                                                                       message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Product is added to cart successfully"] preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                           style:UIAlertActionStyleDefault
                                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                                             
                                                                         }];
                        [alert addAction:okButton];
                        
                        //[wishlistArray replaceObjectAtIndex:indexPath.row withObject:@"1"];
                        //[_productsCollectionView reloadData];
                        [self presentViewController:alert animated:YES completion:^{
                            
                            self.tabBarController.selectedIndex= 1;
                            NSInteger tabitem = self.tabBarController.selectedIndex;
                            [[self.tabBarController.viewControllers objectAtIndex:tabitem] popToRootViewControllerAnimated:YES];
                            
                        }];
                        //productsCountArray=[dataDictionary objectForKey:@"data"];
                        
                        
                    });
                }else {
                    
                    dispatch_async(dispatch_get_main_queue(), ^{
                        
                        [SVProgressHUD dismiss];
                        [[SharedClass sharedInstance]showAlertWithTitle:@"Error!" withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
                    });
                }
                
            }];
            
        }else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"]
                                        
                                                                           message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Please enter product quantity"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 
                                                                 //[self.navigationController popViewControllerAnimated:YES];
                                                                 
                                                             }];
            [alert addAction:okButton];
            [self presentViewController:alert animated:YES completion:nil];
            
            
        }


    }
    
}

-(void)cartBadgeValueServiceCall
{
    //http://voliveafrica.com/makeup_services/services/total_cart_items?customer_id=31&store_id=1
    NSMutableDictionary * cartBadgePostDictionary = [[NSMutableDictionary alloc]init];
    
    [cartBadgePostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"customerId"]forKey:@"customer_id"];
    [cartBadgePostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"saveStore"]forKey:@"store_id"];
    //[[SharedClass sharedInstance]showprogressfor:@"Loading.."];
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"total_cart_items?" withPostDict:cartBadgePostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        //NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@",dataDictionary);
        [SVProgressHUD dismiss];
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                cartBadgeString=[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"total"]];
                
                
                NSLog(@"Badge Value Is%@",cartBadgeString);
                [[[[[self tabBarController] tabBar] items] objectAtIndex:1] setBadgeValue:cartBadgeString];
                
                [[[NSUserDefaults standardUserDefaults]objectForKey:@"total"]objectForKey:@"totalBadge"];
                
            });
            
        } else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                //[objForSharedClass.hud hideAnimated:YES];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[SharedClass sharedInstance]languageSelectedStringForKey:[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
            
        }
        
    }];
    
}



- (IBAction)stepperClicked:(id)sender {
    ANStepperView * stepper = (ANStepperView *)sender;
    stepperValue = [NSString stringWithFormat:@"%f",stepper.value];
    NSLog(@"Stepper Value Is %@",stepperValue);

}
@end
