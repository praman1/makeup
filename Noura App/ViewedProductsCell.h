//
//  ViewedProductsCell.h
//  Noura App
//
//  Created by volive solutions on 24/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"
@interface ViewedProductsCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *productsImagesview;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *presentAmountLabel;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *ratingView;
@property (weak, nonatomic) IBOutlet UILabel *oldAmountLabel;
@end
