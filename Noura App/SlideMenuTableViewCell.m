//
//  SlideMenuTableViewCell.m
//  Noura App
//
//  Created by volive solutions on 23/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "SlideMenuTableViewCell.h"

@implementation SlideMenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
