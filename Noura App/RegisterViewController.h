//
//  RegisterViewController.h
//  Noura App
//
//  Created by volive solutions on 19/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <SVProgressHUD/SVProgressHUD.h>
@interface RegisterViewController : UIViewController<UITextFieldDelegate,UIPickerViewDelegate,UIPickerViewDataSource>
@property (weak, nonatomic) IBOutlet UITextField *nameTF;
@property (weak, nonatomic) IBOutlet UITextField *emailTF;
@property (weak, nonatomic) IBOutlet UITextField *passwordTF;
@property (weak, nonatomic) IBOutlet UITextField *confirmPasswordTF;
@property (weak, nonatomic) IBOutlet UITextField *mobileNumberTF;
@property (weak, nonatomic) IBOutlet UITextField *countryCode_TF;
- (IBAction)countryCodeDrop_BTN:(id)sender;


- (IBAction)signUpBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIImageView *maleCheckBox;
@property (weak, nonatomic) IBOutlet UIImageView *femaleCheckBox;
- (IBAction)alreadyAMemberBtn:(id)sender;
@property (weak, nonatomic) IBOutlet UIScrollView *signUpScrollview;
- (IBAction)maleButton:(id)sender;
- (IBAction)femaleButton:(id)sender;
@property (weak, nonatomic) IBOutlet UISwitch *termsSwitch;
@property (weak, nonatomic) IBOutlet UIButton *signUpBtn;
@property (weak, nonatomic) IBOutlet UILabel *maleLabel;
@property (weak, nonatomic) IBOutlet UILabel *femaleLabel;
@property (weak, nonatomic) IBOutlet UIButton *alreadyAMemberBtn;
@property (weak, nonatomic) IBOutlet UIView *signupView;

@property (weak, nonatomic) IBOutlet UIImageView *logoImageView;
@end
