//
//  RegisterViewController.m
//  Noura App
//
//  Created by volive solutions on 19/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "RegisterViewController.h"
#import "LoginViewController.h"
#import "SWRevealViewController.h"
#import "SharedClass.h"
#import "LineTextField.h"
#import "HeaderAppConstant.h"


@interface RegisterViewController ()
{
    NSString *genderString;
    BOOL isChecked;
    
    UIPickerView *countryPickerView;
    NSMutableArray * countryArray;
    NSString *  selected_Type;
    NSString * status,*combineString;
    NSString * message;
    UIToolbar *toolbar1;
    SharedClass *obj;

    
   // SharedClass *objForSharedclass;
}

@end

@implementation RegisterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self loadViewWithCustomDesign];
    [self pickerView];
    
}

-(void)viewWillAppear:(BOOL)animated{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.navigationController setNavigationBarHidden:YES animated:YES];   //it hides
        
        [UIView animateWithDuration:0 animations:^{
            
            self.logoImageView.transform = CGAffineTransformMakeScale(0.01, 0.01);
            
        }completion:^(BOOL finished){
            
            // Finished scaling down imageview, now resize it
            
            [UIView animateWithDuration:0.4 animations:^{
                
                self.logoImageView.transform = CGAffineTransformIdentity;
                
            }completion:^(BOOL finished){
                
                
            }];
            
            
        }];
        
    });

}




-(void)loadViewWithCustomDesign {

    obj = [[SharedClass alloc] init];
    
    isChecked = NO;
    _nameTF.delegate = self;
    _emailTF.delegate = self;
    _passwordTF.delegate = self;
    _confirmPasswordTF.delegate = self;
    _mobileNumberTF.delegate = self;
    
    countryArray = [[NSMutableArray alloc]initWithObjects:@"+966",@"+973",@"+965",@"+968",@"+974",@"+971", nil];
    countryPickerView = [[UIPickerView alloc] init];
    
    countryPickerView.delegate = self;
    countryPickerView.dataSource = self;
   // _countryCode_TF.inputView = countryPickerView;
    
//    toolbar1= [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, 44)];
//    toolbar1.barStyle = UIBarStyleBlackOpaque;
//    toolbar1.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//    toolbar1.backgroundColor=[UIColor whiteColor];
//    
//    UIBarButtonItem *doneButton1 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Done"]  style: UIBarButtonItemStyleDone target: self action: @selector(show)];
//    [doneButton1 setTintColor:[UIColor whiteColor]];
//    UIBarButtonItem* flexibleSpace1= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
//    UIBarButtonItem *cancelButton1 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Cancel"]  style: UIBarButtonItemStyleDone target: self action: @selector(cancel)];
//    [cancelButton1 setTintColor:[UIColor whiteColor]];
//    toolbar1.items = [NSArray arrayWithObjects:cancelButton1,flexibleSpace1,doneButton1, nil];
//    [_countryCode_TF setInputAccessoryView:toolbar1];

    LineTextField *line = [[LineTextField alloc]init];
    
    UITapGestureRecognizer * tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textFieldDidEndEditing:)];
    [_signupView addGestureRecognizer:tap];
    [_signUpScrollview addGestureRecognizer:tap];
    
    
    [line textfieldAsLine:_nameTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Name"] myView:self.view];
    [line textfieldAsLine:_emailTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Email Address"] myView:self.view];
    [line textfieldAsLine:_passwordTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Password"] myView:self.view];
    [line textfieldAsLine:_confirmPasswordTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Confirm Password"] myView:self.view];
    [line textfieldAsLine:_mobileNumberTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Mobile Number"] myView:self.view];
    [line textfieldAsLine:_countryCode_TF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"+966"] myView:self.view];
    
    int selectedLanguage = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    
    if(selectedLanguage == 2){
       // _logoImageView.image = [UIImage imageNamed:@"New Logo(En)"];
        //Saudi Arabia, Kuwait, the United Arab Emirates, Qatar, Bahrain, and Oman
        _nameTF.textAlignment = NSTextAlignmentRight;
        _emailTF.textAlignment = NSTextAlignmentRight;
        _passwordTF.textAlignment = NSTextAlignmentRight;
        _confirmPasswordTF.textAlignment = NSTextAlignmentRight;
        _mobileNumberTF.textAlignment = NSTextAlignmentRight;
        _countryCode_TF.textAlignment = NSTextAlignmentRight;
       // countryArray = [[NSMutableArray alloc]initWithObjects:@"",@"",@"",@"",@"",@"", nil];
        
    } else  {
       //_logoImageView.image = [UIImage imageNamed:@"New Logo(Ar)"];
        _nameTF.textAlignment = NSTextAlignmentLeft;
        _emailTF.textAlignment = NSTextAlignmentLeft;
        _passwordTF.textAlignment = NSTextAlignmentLeft;
        _confirmPasswordTF.textAlignment = NSTextAlignmentLeft;
        _mobileNumberTF.textAlignment = NSTextAlignmentLeft;
        _countryCode_TF.textAlignment = NSTextAlignmentLeft;
    }
    
    _signUpBtn.layer.masksToBounds = false;
    _signUpBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _signUpBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _signUpBtn.layer.shadowRadius = 3;
    _signUpBtn.layer.shadowOpacity = 0.5;
    
    
    _maleLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Male"];
    _femaleLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"FeMale"];
    
    [_signUpBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"SignUp"] forState:UIControlStateNormal];
    [_alreadyAMemberBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Already A Member? Sign In"] forState:UIControlStateNormal];


}

-(void)pickerView
{
    _countryCode_TF.inputView = countryPickerView;

    toolbar1= [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, 44)];
    toolbar1.barStyle = UIBarStyleBlackOpaque;
    toolbar1.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    toolbar1.backgroundColor=[UIColor whiteColor];
    
    UIBarButtonItem *doneButton1 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Done"]  style: UIBarButtonItemStyleDone target: self action: @selector(show)];
    [doneButton1 setTintColor:[UIColor whiteColor]];
    UIBarButtonItem* flexibleSpace1= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *cancelButton1 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Cancel"]  style: UIBarButtonItemStyleDone target: self action: @selector(cancel)];
    [cancelButton1 setTintColor:[UIColor whiteColor]];
    toolbar1.items = [NSArray arrayWithObjects:cancelButton1,flexibleSpace1,doneButton1, nil];
    [_countryCode_TF setInputAccessoryView:toolbar1];

}



#pragma mark - textfield delegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _nameTF) {
        
        [_signUpScrollview setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
        
    }else if (textField == _emailTF) {
        
        [_signUpScrollview setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
        
    }else if (textField == _passwordTF) {
        
        [_signUpScrollview setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
        
    }else if (textField == _confirmPasswordTF) {
        
        [_signUpScrollview setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
        
    }else if (textField == _mobileNumberTF) {
        
        [_signUpScrollview setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
        
    }else if (textField == _countryCode_TF) {
        
        [_signUpScrollview setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
        
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    [_nameTF resignFirstResponder];
    [_emailTF resignFirstResponder];
    [_passwordTF resignFirstResponder];
    [_confirmPasswordTF resignFirstResponder];
    [_mobileNumberTF resignFirstResponder];
    [_countryCode_TF resignFirstResponder];
    
    [_signUpScrollview setContentOffset:CGPointMake(0,0) animated:YES];
    
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    BOOL returnValue = NO;
    
    if (textField == _nameTF) {
        
        [_emailTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _emailTF) {
        
        [_passwordTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _passwordTF) {
        
        [_confirmPasswordTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _confirmPasswordTF) {
        
        [_mobileNumberTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _mobileNumberTF) {
        
        [_countryCode_TF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _countryCode_TF) {
        
        [_countryCode_TF resignFirstResponder];
        
        returnValue = YES;
        
    }
    
    return returnValue;
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)countryCodeDrop_BTN:(id)sender {
   // [self pickerView];
}

- (IBAction)signUpBtn:(id)sender {
    
    
    [_nameTF resignFirstResponder];
    [_emailTF resignFirstResponder];
    [_passwordTF resignFirstResponder];
    [_confirmPasswordTF resignFirstResponder];
    [_mobileNumberTF resignFirstResponder];
    [_countryCode_TF resignFirstResponder];
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
    
    
    if (self.nameTF.text.length < 3 ) {
        
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Please enter a valid name"] onViewController:self completion:^{  [self.nameTF becomeFirstResponder]; }];
        
    } else if ([emailTest evaluateWithObject:self.emailTF.text] == NO ) {
        
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Please enter valid email id"] onViewController:self completion:^{  [self.emailTF becomeFirstResponder]; }];
        
    } else if (self.passwordTF.text.length < 5 ) {
        
       [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Password should be more than 5 characters"] onViewController:self completion:^{  [self.passwordTF becomeFirstResponder]; }];
        
    } else if (![self.confirmPasswordTF.text isEqualToString:self.passwordTF.text] ) {
        
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Password and Confirm Password must be same"] onViewController:self completion:^{  [self.confirmPasswordTF becomeFirstResponder]; }];
        
    } else if (self.mobileNumberTF.text.length < 5 ) {
        
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Mobile number should be more than 5 digits"] onViewController:self completion:^{  [self.mobileNumberTF becomeFirstResponder]; }];
        
    }else if (self.countryCode_TF.text.length == 0 ) {
        
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Please select country code"] onViewController:self completion:^{  [self.mobileNumberTF becomeFirstResponder]; }];
        
    } else {
        
        [self serviceForRegistration];
        
    }
    
}

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if (pickerView == countryPickerView) {
        return 1;
    }
    
    return 0;
}

// #4
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == countryPickerView) {
        return [countryArray count];
    }
    
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == countryPickerView) {
        selected_Type = countryArray[row];
    }
    
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    if (pickerView == countryPickerView) {
        return countryArray[row];
    }
    
    return nil;
    
}

//Saudi Arabia, Kuwait, the United Arab Emirates, Qatar, Bahrain, and Oman


#pragma mark ServiceForRegistration
-(void)serviceForRegistration {
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];

    NSString *url = @"http://voliveafrica.com/makeup_services/services/registration";
   // NSString * url = [NSString stringWithFormat:@"%@/registration",base_URL];
        
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    NSMutableDictionary * registerPostDictionary = [[NSMutableDictionary alloc]init];
        
    [registerPostDictionary setObject:_emailTF.text forKey:@"email"];
    [registerPostDictionary setObject:_passwordTF.text forKey:@"password"];
    [registerPostDictionary setObject:_nameTF.text forKey:@"name"];
  // [registerPostDictionary setObject:_mobileNumberTF.text forKey:@"mobile"];
    [registerPostDictionary setObject:_countryCode_TF forKey:@"country"];
    combineString = [NSString stringWithFormat:@"%@-%@",_countryCode_TF.text,_mobileNumberTF.text];
    [registerPostDictionary setObject:combineString forKey:@"mobile"];
    [registerPostDictionary setObject:languageStr forKey:@"store_id"];
    
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
        
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
    // post body
    NSMutableData *body = [NSMutableData data];
        
    // add params (all params are strings)
    for (NSString *param in registerPostDictionary) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [registerPostDictionary objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
    }
        
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
        
        
    NSURLSession *session = [NSURLSession sharedSession];
    NSLog(@"The request is %@",request);
        
    [[session dataTaskWithRequest:request completionHandler:^(NSData *  _Nullable data, NSURLResponse *_Nullable response, NSError * _Nullable error) {
            
        NSDictionary *statusDict = [[NSDictionary alloc]init];
        statusDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"Images from server %@", statusDict);
            
        status = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"status"]];
            
        if ([status isEqualToString:@"1"]) {
            
             dispatch_async(dispatch_get_main_queue(), ^{
                 [SVProgressHUD dismiss];
            [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Alert!"] withMessage:[statusDict objectForKey:@"message"] onViewController:self completion:^{  [self.navigationController popViewControllerAnimated:TRUE];}];
             });
                 
            
                    
        } else {
         dispatch_async(dispatch_get_main_queue(), ^{
             [SVProgressHUD dismiss];
             [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Alert!"] withMessage:[statusDict objectForKey:@"message"] onViewController:self completion:^{  [self.nameTF becomeFirstResponder]; }];
         });
            
        }
        
        
        
    }]
         
resume];
        
        
}

-(void)show
{
    //    countryPickerView.hidden = YES;
    //    toolbar1.hidden = YES;
    if(selected_Type.length == 0)
    {
        _countryCode_TF.text = [countryArray objectAtIndex:0];
        
    }  else
    {
        _countryCode_TF.text = selected_Type;
    }
    
    NSLog(@"myTextValue *** %@",_countryCode_TF.text);
    
    
    [_countryCode_TF resignFirstResponder];
    
}
-(void)cancel
{
    [_countryCode_TF resignFirstResponder];
    
}



- (IBAction)alreadyAMemberBtn:(id)sender {
//    LoginViewController *logIn =[self.storyboard instantiateViewControllerWithIdentifier:@"LoginViewController"];
    [self.navigationController popViewControllerAnimated:YES];
}
- (IBAction)maleButton:(id)sender {
     isChecked=!isChecked;
    if(isChecked == YES){
    _maleCheckBox.image = [UIImage imageNamed:@"check2"];
    _femaleCheckBox.image = [UIImage imageNamed:@"checkbox"];
    }
    genderString = [NSString stringWithFormat:@"1"];
}

- (IBAction)femaleButton:(id)sender {
    isChecked=!isChecked;
    if(isChecked == YES){
    _femaleCheckBox.image = [UIImage imageNamed:@"check2"];
    _maleCheckBox.image = [UIImage imageNamed:@"checkbox"];
    }
    genderString = [NSString stringWithFormat:@"2"];
}
@end
