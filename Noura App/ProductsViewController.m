//
//  ProductsViewController.m
//  Noura App
//
//  Created by volive solutions on 28/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "ProductsViewController.h"
#import "ProductsCell.h"
#import "ProductInfoViewController.h"
#import "TabCell.h"
#import "SharedClass.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "HCSStarRatingView.h"
#import "ProductInfoViewController.h"
#import "CartController.h"
@interface ProductsViewController ()
{
     NSMutableArray * selectUnselect_Array;
     NSMutableArray *productsCountArray;
     NSMutableArray *productRatingArray;
     NSMutableArray *productPriceArray;
    NSMutableArray *productIdArray;
    NSMutableArray *clothsArray;
    NSMutableArray *idsArray;
    NSMutableArray *clothsIdArray;
     NSMutableArray *wishlistArray;
    NSMutableArray * productidArr;
    NSMutableArray * filterCheckingArr;
     NSString * productStr;
    
    NSString * wishListStr;
    NSString *sub_cat_id;
    SharedClass *objForSharedClass;
    BOOL wishlist;
}

@end

@implementation ProductsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadViewWithCustomDesign];
    

    //[self productsServiceCall];
    // Do any additional setup after loading the view.
    
}


-(void)viewWillAppear:(BOOL)animated
{
    if ([_search isEqualToString:@"search"]) {
        
        [self searchServiceCall];
    }else
    {
        [self serviceCall:sub_cat_id];
    }
}




-(void)loadViewWithCustomDesign {
    if (_clothNameArr.count>0) {
        self.collectionViewHeightConstraint.constant=51;
    }else{
        self.collectionViewHeightConstraint.constant=0;

    }
    
    objForSharedClass = [[SharedClass alloc] init];
    
    NSLog(@"ProductsViewController.h %@",self.sub_cat_idArr);
    wishlist = NO;
    
    //self.tabNames = [[NSMutableArray alloc]initWithObjects:@"Dresses",@"Tops & Tees",@"Sweaters",@"Jeans",@"Pants",@"Skirts",@"Active Wear",@"Swimsuits",@"Coats & Jackets", nil];
    productsCountArray = [NSMutableArray new];
    self.imagesArr = [NSMutableArray new];
    self.namesArr = [NSMutableArray new];
    productRatingArray = [NSMutableArray new];
    productPriceArray = [NSMutableArray new];
    productIdArray = [NSMutableArray new];
    wishlistArray =[NSMutableArray new];
    filterCheckingArr =[NSMutableArray new];
    
    
    [_tabBarCollectionView reloadData];
    
    selectUnselect_Array = [[NSMutableArray  alloc]initWithObjects:@"1",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0", nil];
    filterCheckingArr=[[NSMutableArray alloc]initWithObjects:@"0",@"0",@"0", nil];
    
    
    UIBarButtonItem *back = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"back"] style:UIBarButtonItemStylePlain target:self action:@selector(backBtnClicked)];
    back.tintColor = [UIColor blackColor];
    self.navigationItem.leftBarButtonItem = back;
    
    UIBarButtonItem *filter = [[UIBarButtonItem alloc]initWithImage:[UIImage imageNamed:@"filters"] style:UIBarButtonItemStylePlain target:self action:@selector(filtersClicked)];
    filter.tintColor = [UIColor blackColor];
    self.navigationItem.rightBarButtonItem = filter;
    
//    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 257, 43)];
//    view.backgroundColor = [UIColor clearColor];
//    
//    UIImageView *imagev = [[UIImageView alloc]initWithFrame:CGRectMake(5, 3, 80, 25)];
//    imagev.image = [UIImage imageNamed:@""];
//    [view addSubview:imagev];
//    self.navigationItem.titleView = view;

    
}



-(void)addWishlistservice:(id)sender{
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    
    
    //[objForSharedClass alertforLoading:self];

    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.productsCollectionView];
    NSIndexPath *indexPath = [self.productsCollectionView indexPathForItemAtPoint:btnPosition];
    // http://voliveafrica.com/noura_services/services/add_or_remove_wishlist?add_or_remove=add&product_id=1&store_id=1&customer_id=39
    NSMutableDictionary *wishListPostDictionary = [[NSMutableDictionary alloc]init];
    [wishListPostDictionary setObject:@"add" forKey:@"add_or_remove"];
    [wishListPostDictionary setObject:[productIdArray objectAtIndex:indexPath.row]forKey:@"product_id"];
    [wishListPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@"customer_id"];
    [wishListPostDictionary setObject:languageStr forKey:@"store_id"];
     //[wishListPostDictionary setObject:@"1" forKey:@"store_id"];
    
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"add_or_remove_wishlist?" withPostDict:wishListPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        //NSString * wishListValue = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                // [objForSharedClass.hud hideAnimated:YES];
                //[hud hideAnimated:YES];
                [SVProgressHUD dismiss];
                
            });
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //[objForSharedClass.hud hideAnimated:YES];
                NSLog(@"Product successfully added to wishlist******");
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
                                            
                                                                               message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Product successfully added to wishlist"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     
                                                                 }];
                [alert addAction:okButton];
                
                [wishlistArray replaceObjectAtIndex:indexPath.row withObject:@"1"];
                [_productsCollectionView reloadData];
                [self presentViewController:alert animated:YES completion:^{
                }];
                //productsCountArray=[dataDictionary objectForKey:@"data"];
                
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //[objForSharedClass.hud hideAnimated:YES];
                [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];

}
-(void)removewishlistService:(id)sender{
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    
   // [objForSharedClass alertforLoading:self];
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.productsCollectionView];
    NSIndexPath *indexPath = [self.productsCollectionView indexPathForItemAtPoint:btnPosition];
    // http://voliveafrica.com/noura_services/services/add_or_remove_wishlist?add_or_remove=add&product_id=1&store_id=1&customer_id=39
    NSMutableDictionary *wishListPostDictionary = [[NSMutableDictionary alloc]init];
    [wishListPostDictionary setObject:@"remove" forKey:@"add_or_remove"];
    [wishListPostDictionary setObject:[productIdArray objectAtIndex:indexPath.row]forKey:@"product_id"];
    [wishListPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@"customer_id"];
    [wishListPostDictionary setObject:languageStr forKey:@"store_id"];
     //[wishListPostDictionary setObject:@"1" forKey:@"store_id"];
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"add_or_remove_wishlist?" withPostDict:wishListPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        //NSString * wishListValue = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                // [objForSharedClass.hud hideAnimated:YES];
                //[hud hideAnimated:YES];
                [SVProgressHUD dismiss];
                
            });
            dispatch_async(dispatch_get_main_queue(), ^{
                //[objForSharedClass.hud hideAnimated:YES];
                NSLog(@"Product successfully added to wishlist******");
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
                                            
                                                                               message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Product successfully removed from wishlist"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     
                                                                 }];
                [alert addAction:okButton];
                
                [wishlistArray replaceObjectAtIndex:indexPath.row withObject:@"0"];
                [_productsCollectionView reloadData];
                [self presentViewController:alert animated:YES completion:^{
                }];
                //productsCountArray=[dataDictionary objectForKey:@"data"];
                
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //[objForSharedClass.hud hideAnimated:YES];
                [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    
    

    
}
-(void)addToWishListServiceCall:(id)sender
{
    
    
    CGPoint btnPosition = [sender convertPoint:CGPointZero toView:self.productsCollectionView];
    NSIndexPath *indexPath = [self.productsCollectionView indexPathForItemAtPoint:btnPosition];

    
    if ([[NSString stringWithFormat:@"%@",[wishlistArray objectAtIndex:indexPath.row]] isEqualToString:@"0"]) {
        
        [self addWishlistservice:sender];
    } else {
        
        [self removewishlistService:sender];
    }
    
}
    
-(void)serviceCall:(NSString *)str_id{
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    // http://voliveafrica.com/noura_services/services/products?cat_id=24&sub_cat_id=3&store_id=1
      //[objForSharedClass alertforLoading:self];
    sub_cat_id = str_id;
    [_imagesArr removeAllObjects];
    [_namesArr removeAllObjects];
    [productPriceArray removeAllObjects];
    [productRatingArray removeAllObjects];
    [productIdArray removeAllObjects];
    [wishlistArray removeAllObjects];
    NSMutableDictionary *productsPostDictionary = [[NSMutableDictionary alloc]init];
    [productsPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"selected_cat_id"]forKey:@"cat_id"];
    [productsPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@"cust_id"];
    [productsPostDictionary setObject:str_id forKey:@"sub_cat_id"];
    [productsPostDictionary setObject:languageStr forKey:@"store_id"];
     //[productsPostDictionary setObject:@"1" forKey:@"store_id"];
    [[SharedClass sharedInstance]fetchResponseforParameter:@"products?" withPostDict:productsPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
       // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                // [objForSharedClass.hud hideAnimated:YES];
                //[hud hideAnimated:YES];
                [SVProgressHUD dismiss];
                
            });
            dispatch_async(dispatch_get_main_queue(), ^{
                //[objForSharedClass.hud hideAnimated:YES];
                productsCountArray=[dataDictionary objectForKey:@"data"];
                
                if (productsCountArray.count>0) {
                    
                    
//                    _imagesArr =[[NSMutableArray alloc]init];
//                    _namesArr =[[NSMutableArray alloc]init];
//                    productPriceArray =[[NSMutableArray alloc]init];
//                    productRatingArray =[[NSMutableArray alloc]init];
//                    productIdArray =[[NSMutableArray alloc]init];
                    
                    
                    
                    for (int i=0; i<productsCountArray.count; i++) {
                        [_imagesArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"image"]];
                        [_namesArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                        [productPriceArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"price"]];
                        [productRatingArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"rating"]];
                        [productIdArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"]];
                        [[NSUserDefaults standardUserDefaults] setObject:productIdArray forKey:@"idArray"];
                        
                        [wishlistArray addObject:[NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"whislist"]]];
                        
                        
                        
                        // NSLog(@"%@",_clothsList);
                    }
                }
                [_productsCollectionView reloadData];
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
               //[objForSharedClass.hud hideAnimated:YES];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    
    
    
}


-(void)searchServiceCall
{
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
     http://voliveafrica.com/makeup_services/services/search_product?search=Toma%20kimas&store_id=1&cust_id=31
    //[objForSharedClass alertforLoading:self];
    [_imagesArr removeAllObjects];
    [_namesArr removeAllObjects];
    [productPriceArray removeAllObjects];
    [productRatingArray removeAllObjects];
    [productIdArray removeAllObjects];
    [wishlistArray removeAllObjects];
    NSMutableDictionary *productsPostDictionary = [[NSMutableDictionary alloc]init];
    
    
    [productsPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@"cust_id"];
    [productsPostDictionary setObject:_textString forKey:@"search"];
    [productsPostDictionary setObject:languageStr forKey:@"store_id"];
    //[productsPostDictionary setObject:@"1" forKey:@"store_id"];
    [[SharedClass sharedInstance]fetchResponseforParameter:@"search_product?" withPostDict:productsPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                // [objForSharedClass.hud hideAnimated:YES];
                //[hud hideAnimated:YES];
                [SVProgressHUD dismiss];
                
            });
            dispatch_async(dispatch_get_main_queue(), ^{
                //[objForSharedClass.hud hideAnimated:YES];
                productsCountArray=[dataDictionary objectForKey:@"data"];
                
                if (productsCountArray.count>0) {
                    
                    
                    //                    _imagesArr =[[NSMutableArray alloc]init];
                    //                    _namesArr =[[NSMutableArray alloc]init];
                    //                    productPriceArray =[[NSMutableArray alloc]init];
                    //                    productRatingArray =[[NSMutableArray alloc]init];
                    //                    productIdArray =[[NSMutableArray alloc]init];
                    
                    
                    
                    for (int i=0; i<productsCountArray.count; i++) {
                        [_imagesArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"image"]];
                        [_namesArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                        [productPriceArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"price"]];
                        [productRatingArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"rating"]];
                        [productIdArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"]];
                        [[NSUserDefaults standardUserDefaults] setObject:productIdArray forKey:@"idArray"];
                        
                        [wishlistArray addObject:[NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"whislist"]]];
                        
                        
                        
                        // NSLog(@"%@",_clothsList);
                    }
                }
                [_productsCollectionView reloadData];
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //[objForSharedClass.hud hideAnimated:YES];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    

}




-(void)filterProductsServiceCall:(NSString *)str_id
{
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    //[objForSharedClass alertforLoading:self];
    sub_cat_id = str_id;
   //http://voliveafrica.com/makeup_services/services/filter_products?cat_id=11&most_selling_product=0&hightolow=1&lowtohigh=0&store_id=1&cust_id=35
    
    NSMutableDictionary *filtersPostDictionary = [[NSMutableDictionary alloc]init];
    [filtersPostDictionary setObject:str_id forKey:@"cat_id"];
    [filtersPostDictionary setObject:[filterCheckingArr objectAtIndex:0] forKey:@"most_selling_product"];
    [filtersPostDictionary setObject:[filterCheckingArr objectAtIndex:1] forKey:@"hightolow"];
    [filtersPostDictionary setObject:[filterCheckingArr objectAtIndex:2] forKey:@"lowtohigh"];
    [filtersPostDictionary setObject:languageStr forKey:@"store_id"];
     //[filtersPostDictionary setObject:@"1" forKey:@"store_id"];
    
    [filtersPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@"cust_id"];
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"filter_products?" withPostDict:filtersPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
       // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"] )
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                // [objForSharedClass.hud hideAnimated:YES];
                //[hud hideAnimated:YES];
                [SVProgressHUD dismiss];
                
            });
            dispatch_async(dispatch_get_main_queue(), ^{
                
                productsCountArray=[dataDictionary objectForKey:@"data"];
                
                if (![productsCountArray isKindOfClass:[NSArray class]]) {
                    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Data is not available"] preferredStyle:UIAlertControllerStyleAlert];
                    UIAlertAction *name = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action) {
                    }];
                    [alertController addAction:name];
                    [self presentViewController:alertController animated:YES completion:nil];
                }
                else{
                    if (productsCountArray.count>0) {
                        
                        _imagesArr =[[NSMutableArray alloc]init];
                        _namesArr =[[NSMutableArray alloc]init];
                        productPriceArray =[[NSMutableArray alloc]init];
                        productRatingArray =[[NSMutableArray alloc]init];
                        productIdArray =[[NSMutableArray alloc]init];
                        
                        
                        
                        for (int i=0; i<productsCountArray.count; i++) {
                            [_imagesArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"image"]];
                            [_namesArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                            [productPriceArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"price"]];
                            [productRatingArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"rating"]];
                            [productIdArray addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"]];
                            [[NSUserDefaults standardUserDefaults] setObject:productIdArray forKey:@"idArray"];
                            
                            [wishlistArray addObject:[NSString stringWithFormat:@"%@",[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"whislist"]]];
                            
                            
                            
                            // NSLog(@"%@",_clothsList);
                        }
                    }
                }
                
                    
                
                
                 dispatch_async(dispatch_get_main_queue(), ^{
                [_productsCollectionView reloadData];
                     //[objForSharedClass.hud hideAnimated:YES];
                 });

            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                //[objForSharedClass.hud hideAnimated:YES];
                //[SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];

    
}


-(void)backBtnClicked
{
    [self.navigationController popViewControllerAnimated:YES];
}


-(void)filtersClicked
{
    UIAlertController *alertController = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Sort By"] message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *name = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Best Seller"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        
        [filterCheckingArr replaceObjectAtIndex:0 withObject:@"1"];
        [filterCheckingArr replaceObjectAtIndex:1 withObject:@"0"];
        [filterCheckingArr replaceObjectAtIndex:2 withObject:@"0"];
        [self filterProductsServiceCall:sub_cat_id];
        
    }];
    UIAlertAction *bestSeller = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Price - high to low"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [filterCheckingArr replaceObjectAtIndex:0 withObject:@"0"];
        [filterCheckingArr replaceObjectAtIndex:1 withObject:@"1"];
        [filterCheckingArr replaceObjectAtIndex:2 withObject:@"0"];
        [self filterProductsServiceCall:sub_cat_id];
        
    }];
    UIAlertAction *topRated = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Price - low to high"] style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        [filterCheckingArr replaceObjectAtIndex:0 withObject:@"0"];
        [filterCheckingArr replaceObjectAtIndex:1 withObject:@"0"];
        [filterCheckingArr replaceObjectAtIndex:2 withObject:@"1"];
        [self filterProductsServiceCall:sub_cat_id];
        
        
    }];
    //UIAlertAction *latest = [UIAlertAction actionWithTitle:@"Latest" style:UIAlertActionStyleDefault handler:nil];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:name];
    [alertController addAction:bestSeller];
    [alertController addAction:topRated];
    
   // [controller addAction:latest];
    [alertController addAction:cancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
    

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    if (collectionView == _productsCollectionView) {
        CGFloat width = (CGFloat) (_productsCollectionView.frame.size.width/2);
        
        return CGSizeMake(width-5,240);
    }
    else{
                CGFloat width = (CGFloat) (_tabBarCollectionView.frame.size.width/3);
        
                return CGSizeMake(width-5,240);
        
        
        //        CGSize frameOfLbl = [skillWithCost boundingRectWithSize:rectForLabel.size options:NSStringDrawingUsesLineFragmentOrigin attributes:@{ NSFontAttributeName:self.skillsTF.font} context:nil].size;
        //        widthOfCell = frameOfLbl.width;
        //
        //        NSString * widthStrig = [NSString stringWithFormat:@"%i",widthOfCell];
        //
        //        [self.widthsArray addObject:widthStrig];
       // return CGSizeMake(129, 60);
    }
    
}

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    if (collectionView == _productsCollectionView) {
        return self.namesArr.count;
        
    }
    else{
        return self.clothNameArr.count;
        
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(nonnull NSIndexPath *)indexPath
{
    if (collectionView == _productsCollectionView) {
        ProductsCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ProductsCell" forIndexPath:indexPath];
        
        cell.productRatingView.userInteractionEnabled=NO;
        
        [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[_imagesArr objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"img4"]];
        cell.productNameLabel.text =[_namesArr objectAtIndex:indexPath.row];
        cell.presentAmountLabel.text = [NSString stringWithFormat:@"%@",[productPriceArray objectAtIndex:indexPath.row]];
        cell.productRatingView.value = [[productRatingArray objectAtIndex:indexPath.row]intValue];
        [cell.favourite addTarget:self action:@selector(addToWishListServiceCall:) forControlEvents:UIControlEventTouchUpInside];
        if ([[wishlistArray objectAtIndex:indexPath.row] isEqualToString:@"1"]) {
            NSLog(@"prodcut in wishlist");
            [cell.favourite setImage:[UIImage imageNamed:@"fav"] forState:UIControlStateNormal];
            
        }else{
            NSLog(@"product is not wishlist");
            [cell.favourite setImage:[UIImage imageNamed:@"fav grey"] forState:UIControlStateNormal];
        }
        
        
        return  cell;
    }
    else{
        TabCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TabViewCell" forIndexPath:indexPath];
        
        
        cell.tabName.text = [self.clothNameArr objectAtIndex:indexPath.row];
        
        if ([[selectUnselect_Array objectAtIndex:indexPath.row] isEqualToString:@"1"]) {
            
            cell.tabView.hidden = NO;
            
        } else {
            
            cell.tabView.hidden = YES;
            
        }
        
        
        return  cell;
    }
}
-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
   
   if(collectionView == _productsCollectionView)
   {
                ProductInfoViewController *productsList = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductInfoViewController"];
                [productsList productInfoServiceCall:[productIdArray objectAtIndex:indexPath.row]];
                [self.navigationController pushViewController:productsList animated:YES];
                                            
        
   }else
   {
       
       selectUnselect_Array = [[NSMutableArray  alloc]initWithObjects:@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0",@"0", nil];
       
       [selectUnselect_Array replaceObjectAtIndex:indexPath.row withObject:@"1"];
       
       [self serviceCall:[_sub_cat_idArr objectAtIndex:indexPath.row]];
       
       [_tabBarCollectionView reloadData];
       
       
       
   }

    
}


@end
