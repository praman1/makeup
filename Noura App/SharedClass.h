//
//  SharedClass.h
//  HAWLIK
//
//  Created by murali krishna on 2/6/17.
//  Copyright © 2017 volivesolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>
#import "SCLAlertView.h"
#import "MBProgressHUD.h"
#import "Reachability.h"


//#import <SVProgressHUD/SVProgressHUD.h>
@interface SharedClass : NSObject


typedef void (^ HalikData) ( NSData * dataFromJson );


+(SharedClass *)sharedInstance;

//Network Checking

//-(BOOL)isNetworkAvalible;
@property (strong,nonatomic) Reachability* reachability;
@property (strong , nonatomic) MBProgressHUD *hud;
@property (strong ,nonatomic) NSDictionary *json;
-(void)alertforLoading:(UIViewController *)ve;
-(void)alertforMessage:(UIViewController *)ve;
-(void)showprogressfor:(NSString *)str;

-(void)showprogressforSucsess:(NSString *)str;

-(void)showAlertWithTitle: (NSString *)title withMessage: (NSString *)message onViewController: (UIViewController *)viewController completion:(void (^)(void))completionBlock;

-(void)fetchResponseforParameter:(NSString *)parameterString withPostDict:(NSDictionary *)postDictionary andReturnWith:(HalikData)completionHandler;
-(NSString*)languageSelectedStringForKey:(NSString*) key;

-(UIImage *)drawImage:(UIImage*)profileImage withBadge:(UIImage *)badge;

//methods
-(void)GETList:(NSString *)post completion :(void (^)(NSDictionary *, NSError *))completion;
-(void)postMethodForServicesRequest:(NSString *)serviceType :(NSString *)post completion:(void (^)(NSDictionary *, NSError *))completion;
-(void)alertforMessage:(UIViewController *)ve :(NSString *)imgname :(NSString *)text :(NSString *)buttonTitle;
@end
