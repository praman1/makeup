//
//  TabBarController.m
//  JOTUN
//
//  Created by volive solutions on 2/7/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "TabBarController.h"
#import "AppDelegate.h"
#import "SharedClass.h"
#import "HomeViewController.h"
@interface TabBarController (){
    
    AppDelegate * tabBarAppDelegate;
}

@end

@implementation TabBarController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    tabBarAppDelegate = (AppDelegate *)[[UIApplication sharedApplication]delegate];
    HomeViewController*home=[[HomeViewController alloc]init];
   
    
 
    [[UITabBar appearance] setTintColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1.0]];

   
    
    if ([tabBarAppDelegate.selectTabItem isEqualToString:@"0"]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[SharedClass sharedInstance]alertforMessage:home];
            
        });
        
       // [[UITabBar appearance] setTintColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1.0]];
        [self setSelectedIndex:0];
            } else if ([tabBarAppDelegate.selectTabItem isEqualToString:@"1"]) {
                
                dispatch_async(dispatch_get_main_queue(), ^{
                   
                    [[SharedClass sharedInstance]alertforMessage:self];
                    
                });
                
        //[[UITabBar appearance] setTintColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1.0]];
        [self setSelectedIndex:1];
        
    } else if ([tabBarAppDelegate.selectTabItem isEqualToString:@"2"]) {
        
        dispatch_async(dispatch_get_main_queue(), ^{
            
            [[SharedClass sharedInstance]alertforMessage:home];
            
        });
        
        //[[UITabBar appearance] setTintColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1.0]];
        [self setSelectedIndex:2];
    }else if ([tabBarAppDelegate.selectTabItem isEqualToString:@"3"]) {
                dispatch_async(dispatch_get_main_queue(), ^{
            
            [[SharedClass sharedInstance]alertforMessage:home];
            
        });
        
       // [[UITabBar appearance] setTintColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1.0]];
        [self setSelectedIndex:3];
    }
   
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
