//
//  AppDelegate.h
//  Noura App
//
//  Created by volive solutions on 18/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@property NSString * selectTabItem;
@end

