//
//  ProfileViewController.m
//  Noura App
//
//  Created by volive solutions on 29/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "ProfileViewController.h"
#import "LineTextField.h"
#import "HomeViewController.h"
#import "SlideMenuViewController.h"
#import "SWRevealViewController.h"
#import "SharedClass.h"
#import "LineTextField.h"
@interface ProfileViewController ()
{
    BOOL isChecked;
    NSString * addressStr;
    NSArray *streetArray;
    
    NSString * titleStr;
    NSString * genderString;
    NSString * status1;
    NSString * message1;
    
    BOOL editingProfile;
    
    SharedClass *objforsharedclass;
    
    
}

@end

@implementation ProfileViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadViewWithCustomDesign];
          // Do any additional setup after loading the view.
}

-(void)loadViewWithCustomDesign {

    self.navigationItem.title = [[SharedClass sharedInstance]languageSelectedStringForKey:@"My Account"];
    _addNewAddressBtn.layer.masksToBounds = false;
    _addNewAddressBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _addNewAddressBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _addNewAddressBtn.layer.shadowRadius = 3;
    _addNewAddressBtn.layer.shadowOpacity = 0.5;
    
    objforsharedclass = [[SharedClass alloc] init];
    
    
    editingProfile = NO;
    isChecked = NO;
    
    int selectedLanguage = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    
    if(selectedLanguage == 2){
        
        _firstNameTF.textAlignment = NSTextAlignmentRight;
        _emailTF.textAlignment = NSTextAlignmentRight;
        _lastNameTF.textAlignment = NSTextAlignmentRight;
        _mobileNumberTF.textAlignment = NSTextAlignmentRight;
        
    } else  {
        
        _firstNameTF.textAlignment = NSTextAlignmentLeft;
        _emailTF.textAlignment = NSTextAlignmentLeft;
        _lastNameTF.textAlignment = NSTextAlignmentLeft;
        _mobileNumberTF.textAlignment = NSTextAlignmentLeft;
    }

    
    
    _maleLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Male"];
    _femaleLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"FeMale"];
    //Profile
    _profileHeaderLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Profile"];
    _addressHeading.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Address"];

    [_addNewAddressBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Add a New Address"] forState:UIControlStateNormal];
    
    _firstNameTF.userInteractionEnabled=NO;
    _lastNameTF.userInteractionEnabled=NO;
    _emailTF.userInteractionEnabled=NO;
    _mobileNumberTF.userInteractionEnabled=NO;
    //_addressTextView.userInteractionEnabled=NO;
    _addressTextView.editable=NO;
    _MaleBtn.userInteractionEnabled = NO;
    _femaleBTn.userInteractionEnabled = NO;
    
    
    _profileEditBtn.layer.cornerRadius = _profileEditBtn.frame.size.width/2;
    _profileEditBtn.clipsToBounds = YES;
    
    
    _profilebackview.layer.cornerRadius = 4;
    _addressBackview.layer.cornerRadius = 4;
    _addressBackview.layer.cornerRadius = 4;
    
    
    
    SWRevealViewController *revealViewController = self.revealViewController;
    if ( revealViewController )
    {
        
        [_backBar setTarget: self.revealViewController];
        [_backBar setAction: @selector( revealToggle: )];
        [self.view addGestureRecognizer:self.revealViewController.panGestureRecognizer];
    }
    [revealViewController tapGestureRecognizer];
    
    LineTextField *line = [[LineTextField alloc]init];
    
    [line textfieldAsLine:_firstNameTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"First Name"] myView:self.view];
    [line textfieldAsLine:_lastNameTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Last Name"] myView:self.view];
    [line textfieldAsLine:_emailTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Email Address"] myView:self.view];
    [line textfieldAsLine:_mobileNumberTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Mobile Number"] myView:self.view];
    _nameView.layer.borderColor = [[UIColor whiteColor]CGColor];
    _nameView.layer.cornerRadius = _nameView.frame.size.width/2;
    streetArray = [NSMutableArray new];


}


-(void)viewWillAppear:(BOOL)animated{

    [self profileServiceCall];

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/
#pragma mark - textfield delegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _firstNameTF) {
        
        [_profileScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y+80) animated:YES];
        
    }else if (textField == _lastNameTF) {
        
        [_profileScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y+120) animated:YES];
        
    }else if (textField == _emailTF) {
        
        [_profileScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y+120) animated:YES];
        
    }else if (textField == _mobileNumberTF) {
        
        [_profileScrollView setContentOffset:CGPointMake(0, textField.frame.origin.y+120) animated:YES];
        
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
    
    [_firstNameTF resignFirstResponder];
    [_lastNameTF resignFirstResponder];
    [_emailTF resignFirstResponder];
    [_mobileNumberTF resignFirstResponder];
    [_profileScrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
}


- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
//    BOOL returnValue = NO;
//    
//    if (textField == _firstNameTF) {
//        
//        [_lastNameTF becomeFirstResponder];
//        
//        returnValue = YES;
//        
//    }else if (textField == _lastNameTF) {
//        
//        [_emailTF becomeFirstResponder];
//        
//        returnValue = YES;
//        
//    }else if (textField == _emailTF) {
//        
//        [_mobileNumberTF becomeFirstResponder];
//        
//        returnValue = YES;
//        
//    }else if (textField == _mobileNumberTF) {
//        
//        [_mobileNumberTF resignFirstResponder];
//        
//        returnValue = YES;
//        
//    }
    [textField resignFirstResponder];
    return YES;
}

-(void)profileServiceCall
{
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
   // [objforsharedclass alertforLoading:self];
    
   // http://voliveafrica.com/noura_services/services/user_profile?customer_id=39&store_id=2
    NSMutableDictionary * profilePostDictionary = [[NSMutableDictionary alloc]init];
        [profilePostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"] forKey:@"customer_id"];
    
    // [profilePostDictionary setObject:@"39" forKey:@"customer_id"];customerId
        [profilePostDictionary setObject:languageStr forKey:@"store_id"];
    // [profilePostDictionary setObject:@"1" forKey:@"store_id"];

    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"user_profile?" withPostDict:profilePostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
       // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
            });
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _firstNameTF.text = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]objectForKey:@"first_name"]];
                
                
                _lastNameTF.text = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]objectForKey:@"last_name"]];
                
                _nameLabel.text = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]objectForKey:@"first_name"]];
                NSString* firstNameStr = _firstNameTF.text;
                 NSString* lastNameStr = _lastNameTF.text;
                
                _nameLabel.text = [NSString stringWithFormat:@"%@  %@" ,firstNameStr,lastNameStr ];
                titleStr=_nameLabel.text;
                
                _emailTF.text = [NSString stringWithFormat:@"%@",[[dataDictionary objectForKey:@"data"]objectForKey:@"email"]];
                
                _mobileNumberTF.text = [NSString stringWithFormat:@"%@",[[dataDictionary valueForKey:@"data"] valueForKey:@"mobile"]];
                NSString *stradd = [NSString stringWithFormat:@"%@",[[dataDictionary valueForKey:@"data"] valueForKey:@"address_exits"]];
                
                if (![stradd isEqualToString:@"0"]) {
                    _addressBackview.hidden = NO;
                    _addressHeading.hidden = NO;
                    
                streetArray = [[dataDictionary objectForKey:@"address"]objectForKey:@"street"];
                  NSString *addString = @"";
                    for(int a = 0 ;a <streetArray.count ; a ++){
                        addString = [addString stringByAppendingString:[NSString stringWithFormat:@" %@",streetArray[a]]];
                    
                    }
                
                addressStr=[NSString stringWithFormat:@"%@ ,%@, %@, %@, %@, %@, %@",
                            [[dataDictionary objectForKey:@"address"]objectForKey:@"first_name"],
                            [[dataDictionary objectForKey:@"address"]objectForKey:@"last_name"],
                            [[dataDictionary objectForKey:@"address"]objectForKey:@"city"],
                            [[dataDictionary objectForKey:@"address"]objectForKey:@"postcode"],
                            [[dataDictionary objectForKey:@"address"]objectForKey:@"telephone"],
                            [[dataDictionary objectForKey:@"address"]objectForKey:@"country"],
                addString ];
                
                 [addressStr stringByReplacingOccurrencesOfString:@"\"" withString:@""];
                
                _addressTextView.text=addressStr;
                
               NSCharacterSet *charsToTrim = [NSCharacterSet characterSetWithCharactersInString:@"( ) <null><null><null><null><null> 0 , "" "];
                _addressTextView.text = [addressStr stringByTrimmingCharactersInSet:charsToTrim];
                }else{
                    _addressHeading.hidden = YES;
                    _addressBackview.hidden = YES;
                }
                 NSMutableString * firstCharacters = [NSMutableString string];
                NSArray * words = [titleStr componentsSeparatedByCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
                for (NSString * word in words) {
                    if ([word length] > 1) {
                        NSString * firstLetter = [word substringToIndex:1];
                        [firstCharacters appendString:[firstLetter uppercaseString]];
                       
                        _nameLetterLabel.text=firstCharacters;
                        //[[NSUserDefaults standardUserDefaults]setObject:_nameLetterLabel.text forKey:@"letter"];
                    }
                }
              
                [[NSUserDefaults standardUserDefaults]setObject:[[dataDictionary objectForKey:@"data"]objectForKey:@"cart_id"] forKey:@"CartId"];
                
                //_bannerImagesArray = bannerArray;
                NSLog(@"Mydata %@",_firstNameTF.text);
               // [objforsharedclass.hud hideAnimated:YES];
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //[SVProgressHUD dismiss];
               // [objforsharedclass.hud hideAnimated:YES];
                [[SharedClass sharedInstance]showAlertWithTitle:@"Alert!" withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];

}



- (IBAction)firstNameEditBtn:(id)sender {
}

- (IBAction)lastNameEditBtn:(id)sender {
}
- (IBAction)maleCheckBtn:(id)sender {
//    _maleCheckBox.image = [UIImage imageNamed:@"select radio"];
//    _femaleCheckBox.image = [UIImage imageNamed:@"unselect radio"];
    isChecked=!isChecked;
    if(isChecked == YES){
        _maleCheckBox.image = [UIImage imageNamed:@"select radio"];
        _femaleCheckBox.image = [UIImage imageNamed:@"unselect radio"];
    }
    genderString = [NSString stringWithFormat:@"1"];
    
    
   
}

- (IBAction)femaleCheckBtn:(id)sender {
//    _femaleCheckBox.image = [UIImage imageNamed:@"select radio"];
//    _maleCheckBox.image = [UIImage imageNamed:@"unselect radio"];
    isChecked=!isChecked;
    if(isChecked == YES){
        _maleCheckBox.image = [UIImage imageNamed:@"unselect radio"];
        _femaleCheckBox.image = [UIImage imageNamed:@"select radio"];
    }
    genderString = [NSString stringWithFormat:@"2"];

    
   }
- (IBAction)addressEditBtn:(id)sender {
}

- (IBAction)addANewAddressBtn:(id)sender {
}
- (IBAction)backBarBtn:(id)sender {
   // SWRevealViewController * slideMenu = [self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)editDetailsSrvice
{
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
//    if(_termsSwitch.isOn == YES && isChecked == YES)
//    {
    
        //email,store_id,f_name,l_name,gender
        
        NSString *url = @"http://voliveafrica.com/makeup_services/services/update_profile";
        
        
        NSMutableDictionary * registerPostDictionary = [[NSMutableDictionary alloc]init];
        
       
        [registerPostDictionary setObject:_emailTF.text forKey:@"email"];
        [registerPostDictionary setObject:_firstNameTF.text forKey:@"f_name"];
        [registerPostDictionary setObject:_lastNameTF.text forKey:@"l_name"];
        [registerPostDictionary setObject:_mobileNumberTF.text forKey:@"mobile"];
        
       // [registerPostDictionary setObject:genderString forKey:@"gender"];
        [registerPostDictionary setObject:languageStr forKey:@"store_id"];
     //[registerPostDictionary setObject:@"1" forKey:@"store_id"];
        
        
        NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
        
        NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
        
        // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
        
        [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
        [request setHTTPShouldHandleCookies:NO];
        [request setTimeoutInterval:30];
        [request setHTTPMethod:@"POST"];
        
        // set Content-Type in HTTP header
        NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
        [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
        
        // post body
        NSMutableData *body = [NSMutableData data];
        
        // add params (all params are strings)
        for (NSString *param in registerPostDictionary) {
            [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
            [body appendData:[[NSString stringWithFormat:@"%@\r\n", [registerPostDictionary objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
            [request setHTTPBody:body];
        }
    
        // set the content-length
        NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
        [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
        NSURLSession *session = [NSURLSession sharedSession];
        NSLog(@"The request is %@",request);
        
        [[session dataTaskWithRequest:request completionHandler:^(NSData *  _Nullable data, NSURLResponse *_Nullable response, NSError * _Nullable error) {
            
            NSDictionary *statusDict = [[NSDictionary alloc]init];
            statusDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
            NSLog(@"Images from server %@", statusDict);
           
            status1  = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"status"]];
            
           // message1 = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
            
            if ([status1  isEqualToString:@"1"])
            {
 
              dispatch_async(dispatch_get_main_queue(), ^{
                
                UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
                                            
                                                                               message:[[SharedClass sharedInstance]languageSelectedStringForKey:@" Profile edited Successfully"] preferredStyle:UIAlertControllerStyleAlert];
                
                UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                   style:UIAlertActionStyleDefault
                                                                 handler:^(UIAlertAction * _Nonnull action) {
                                                                     
                                                                     [self.navigationController popViewControllerAnimated:YES];
                                                                     
                                                                 }];
                [alert addAction:okButton];
                  [self presentViewController:alert animated:YES completion:^{
                      
                      _firstNameTF.userInteractionEnabled=NO;
                      _lastNameTF.userInteractionEnabled=NO;
                      _emailTF.userInteractionEnabled=NO;
                      _mobileNumberTF.userInteractionEnabled=NO;
                      _MaleBtn.userInteractionEnabled = NO;
                      _femaleBTn.userInteractionEnabled = NO;
                  
                  }];
                  });
             }
        }]
         
         resume];
}

- (IBAction)editProfileBtnAction:(id)sender {
//   [ _firstNameTF setClearsOnBeginEditing:YES];
    
    editingProfile=!editingProfile;
    
    if (editingProfile == YES) {
        [_profileEditBtn setImage:[UIImage imageNamed:@"save"] forState:UIControlStateNormal];
        [ _firstNameTF setClearsOnBeginEditing:YES];
        [ _lastNameTF setClearsOnBeginEditing:YES];
        [ _emailTF setClearsOnBeginEditing:YES];
        [_mobileNumberTF setClearsOnBeginEditing:YES];

        _firstNameTF.userInteractionEnabled=YES;
        _lastNameTF.userInteractionEnabled=YES;
        _emailTF.userInteractionEnabled=NO;
        _MaleBtn.userInteractionEnabled = YES;
        _femaleBTn.userInteractionEnabled = YES;
        _mobileNumberTF.userInteractionEnabled = YES;
    
    }else
    {
      
        [self editDetailsSrvice];
        [_profileEditBtn setImage:[UIImage imageNamed:@"edit"] forState:UIControlStateNormal];
    
    }

}
@end
