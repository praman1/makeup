//
//  ViewAllCell.h
//  Noura App
//
//  Created by volive solutions on 27/09/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface ViewAllCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *productImageView;
@property (weak, nonatomic) IBOutlet UILabel *productNameLabel;
@property (weak, nonatomic) IBOutlet UILabel *presentAmountLabel;
@property (weak, nonatomic) IBOutlet UILabel *oldAmountLabel;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *productRatingView;

@property (weak, nonatomic) IBOutlet UIButton *favourite;

- (IBAction)favBtnAction:(id)sender;

@end
