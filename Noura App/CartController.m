//
//  CartController.m
//  Noura App
//
//  Created by Mohammad Apsar on 8/30/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "CartController.h"
#import "PaymentController.h"
#import "ConfirmationController.h"
#import "ThankYouOrderViewController.h"
#import "SharedClass.h"
#import "HeaderAppConstant.h"
#import "AddressInCartTableViewCell.h"
#import "LineTextField.h"

@interface CartController ()<UITableViewDelegate,UITableViewDataSource>{
    PaymentController *payment;
    ConfirmationController *confirmation;
    int width;
    int height;
    UIPickerView *countryPickerView,*countryCodePickerView;
    NSMutableArray * countryArr,*countryCodeArray;
    UIToolbar *toolbar1,*toolbar2;
    NSString *  selected_Type,*selected_Type1;
    NSString * status;
    NSDictionary *statusDict;
    
    NSString * label1Str;
    NSString * label2Str;
    NSString * value1Str;
    NSString * value2Str;
    NSString * totalPriceStr;
    SharedClass *objForSharedClass;
    NSString *string,*combinedString;
    NSString * defaultString;
    
    //SharedClass *obj;
    NSMutableArray *ArrForAddresses;
    AddressInCartTableViewCell *cell;
    
    NSMutableArray *ArrForSelection ;
    NSMutableArray *ArrForcompany,*ArrForfirstName,*ArrForlastName,*ArrForaddress,*ArrForcity,*ArrForstate,*ArrForphone,*ArrForpincode,*ArrForcountry;
    
    BOOL isChecked;
    int index;
    
}

@end

@implementation CartController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadViewWithCustomDesign];
    
//    UITapGestureRecognizer *tap1 = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textFieldDidEndEditing:)];
//    [self.scrollView addGestureRecognizer:tap1];
    
   
    
    // Do any additional setup after loading the view.
}

-(void)loadViewWithCustomDesign {
    isChecked = NO;
  //  self.navigationItem.title = @"Wishlist";
    countryCodeArray = [[NSMutableArray alloc]initWithObjects:@"+966",@"+973",@"+965",@"+968",@"+974",@"+971", nil];
    string = @"Yes";
    _continuePayBtn.layer.masksToBounds = false;
    _continuePayBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _continuePayBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _continuePayBtn.layer.shadowRadius = 3;
    _continuePayBtn.layer.shadowOpacity = 0.5;
    int selectedLanguage = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    
    if(selectedLanguage == 2){
        
        _firstName.textAlignment = NSTextAlignmentRight;
        _lastNameTF.textAlignment = NSTextAlignmentRight;
        
        _companyTF.textAlignment = NSTextAlignmentRight;
        _addressTF.textAlignment = NSTextAlignmentRight;
        _phoneTF.textAlignment = NSTextAlignmentRight;
        _cityTF.textAlignment = NSTextAlignmentRight;
        _pincodeTF.textAlignment = NSTextAlignmentRight;
        _stateTF.textAlignment = NSTextAlignmentRight;
        _countryTF.textAlignment = NSTextAlignmentRight;
        _countryCode_TF.textAlignment = NSTextAlignmentRight;
        _countryCodeDropImage2.hidden = YES;
        _countryDropBtn2.hidden = YES;
        countryArr=[[NSMutableArray alloc]initWithObjects:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Saudi Arabia"], nil];
        
        
    } else  {
        
        _firstName.textAlignment = NSTextAlignmentLeft;
        _lastNameTF.textAlignment = NSTextAlignmentLeft;
        _companyTF.textAlignment = NSTextAlignmentLeft;
        _addressTF.textAlignment = NSTextAlignmentLeft;
        _phoneTF.textAlignment = NSTextAlignmentLeft;
        _cityTF.textAlignment = NSTextAlignmentLeft;
        _pincodeTF.textAlignment = NSTextAlignmentLeft;
        _stateTF.textAlignment = NSTextAlignmentLeft;
        _countryTF.textAlignment = NSTextAlignmentLeft;
        _countryCode_TF.textAlignment = NSTextAlignmentLeft;
        _countryCodeDropImage1.hidden = YES;
        _countryDropBtn1.hidden = YES;
        countryArr=[[NSMutableArray alloc]initWithObjects:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Saudi Arabia"], nil];
    }

    
    //combinedString = [_countryCode_TF.text stringByAppendingString:_phoneTF.text];
    
    
    [_segmentController setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"New Address"] forSegmentAtIndex:0];
    [_segmentController setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Saved Address"] forSegmentAtIndex:1];
    
    LineTextField *line = [[LineTextField alloc]init];
    
    [line textfieldAsLine:_firstName lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"First Name"] myView:self.view];
    [line textfieldAsLine:_lastNameTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Last Name"] myView:self.view];
    [line textfieldAsLine:_phoneTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Mobile Number"] myView:self.view];
    [line textfieldAsLine:_companyTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Company"] myView:self.view];
    
    [line textfieldAsLine:_addressTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"StreetAddress1"] myView:self.view];
        [line textfieldAsLine:_cityTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"City"] myView:self.view];
    [line textfieldAsLine:_countryCode_TF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"+973"] myView:self.view];
    [line textfieldAsLine:_stateTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"State"] myView:self.view];
    [line textfieldAsLine:_countryTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Country"] myView:self.view];

    

    [_continuePayBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"CONTINUE TO PAYMENT"] forState:UIControlStateNormal];
    
    _saveAddressLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Save Address"];

    _shippingLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Shipping"];
    _paymentLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Payment"];
    _confirmationLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Confirmation"];
    _shippingAddressLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Shipping Address"];
    
    
    
    //_savedAddressTV.hidden=YES;
    _savedAddressTV.estimatedRowHeight = 500;
    _savedAddressTV.rowHeight = UITableViewAutomaticDimension;
    
    objForSharedClass = [[SharedClass alloc] init];
   // countryArr=[[NSMutableArray alloc]initWithObjects:@"Saudi Arabia", nil];
    
    
    //***Country Picker View***//
    
    countryPickerView = [[UIPickerView alloc] init];
    countryPickerView.delegate = self;
    countryPickerView.dataSource = self;
    _countryTF.inputView = countryPickerView;
    
    toolbar1= [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, 44)];
    toolbar1.barStyle = UIBarStyleBlackOpaque;
    toolbar1.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    toolbar1.backgroundColor=[UIColor whiteColor];
    UIBarButtonItem *doneButton1 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Done"]  style: UIBarButtonItemStyleDone target: self action: @selector(show)];
   // [self.scrollView setContentOffset:CGPointZero animated:YES];
    [doneButton1 setTintColor:[UIColor whiteColor]];
    UIBarButtonItem* flexibleSpace1= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *cancelButton1 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Cancel"]  style: UIBarButtonItemStyleDone target: self action: @selector(cancel)];
    //[self.scrollView setContentOffset:CGPointZero animated:YES];
    [cancelButton1 setTintColor:[UIColor whiteColor]];
    toolbar1.items = [NSArray arrayWithObjects:cancelButton1,flexibleSpace1,doneButton1, nil];
    [_countryTF setInputAccessoryView:toolbar1];
    
    
    //***Country Code Picker View***//
    countryCodePickerView = [[UIPickerView alloc] init];
    countryCodePickerView.delegate = self;
    countryCodePickerView.dataSource = self;
    _countryCode_TF.inputView = countryCodePickerView;
    
    toolbar2= [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, 44)];
    toolbar2.barStyle = UIBarStyleBlackOpaque;
    toolbar2.autoresizingMask = UIViewAutoresizingFlexibleWidth;
    toolbar2.backgroundColor=[UIColor whiteColor];
    UIBarButtonItem *doneButton2 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Done"]  style: UIBarButtonItemStyleDone target: self action: @selector(show1)];
    // [self.scrollView setContentOffset:CGPointZero animated:YES];
    [doneButton1 setTintColor:[UIColor whiteColor]];
    UIBarButtonItem* flexibleSpace2= [[UIBarButtonItem alloc] initWithBarButtonSystemItem:UIBarButtonSystemItemFlexibleSpace target:nil action:nil];
    UIBarButtonItem *cancelButton2 = [[UIBarButtonItem alloc] initWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Cancel"]  style: UIBarButtonItemStyleDone target: self action: @selector(cancel1)];
    //[self.scrollView setContentOffset:CGPointZero animated:YES];
    [cancelButton1 setTintColor:[UIColor whiteColor]];
    toolbar2.items = [NSArray arrayWithObjects:cancelButton2,flexibleSpace2,doneButton2, nil];
    [_countryCode_TF setInputAccessoryView:toolbar2];
    
    
//    toolbar1= [[UIToolbar alloc] initWithFrame: CGRectMake(0, 0, self.view.frame.size.width, 44)];
//    toolbar1.barStyle = UIBarStyleBlackOpaque;
//    toolbar1.autoresizingMask = UIViewAutoresizingFlexibleWidth;
//    toolbar1.backgroundColor=[UIColor whiteColor];
//    UIBarButtonItem *done = [[UIBarButtonItem alloc] initWithTitle: @"Done" style: UIBarButtonItemStyleDone target: self action: @selector(done)];
//    [doneButton1 setTintColor:[UIColor whiteColor]];
//   
//       toolbar1.items = [NSArray arrayWithObjects:done, nil];
//    
//    [_cityTF setInputAccessoryView:toolbar1];

    
    
    width = self.view.frame.size.width;
    height = self.view.frame.size.height;
    
    payment  = [self.storyboard instantiateViewControllerWithIdentifier:@"PaymentController"];
    confirmation = [self.storyboard instantiateViewControllerWithIdentifier:@"ConfirmationController"];
    
    //shipping view
    UITapGestureRecognizer *tap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(shippingBtnClicked)];
    [self.shippingViewBtn addGestureRecognizer:tap];
    
    //payment view
    UITapGestureRecognizer *paymentTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(paymentBtnClicked)];
    [self.paymentViewBtn addGestureRecognizer:paymentTap];
    
    //confirmation view
    UITapGestureRecognizer *confirmationTap = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(confirmationBtnClicked)];
    [self.confirmationViewBtn addGestureRecognizer:confirmationTap];
    
    [self GetSavedAddress];


}

-(void)done
{
     [_cityTF resignFirstResponder];
}
-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [textField resignFirstResponder];
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
 //   BOOL returnValue = NO;
    
    if (textField == _firstName) {
        
        [_lastNameTF becomeFirstResponder];
        
        return YES ;
        
    }else if (textField == _lastNameTF) {
        
        [_countryCode_TF becomeFirstResponder];
        
        return YES;
        
    }else if (textField == _countryCode_TF) {
        
        [_phoneTF becomeFirstResponder];
        
        return YES;
        
    }else if (textField == _phoneTF) {
        
        [_addressTF becomeFirstResponder];
        
        return YES;
        
    }else if (textField == _addressTF) {
        
        [_cityTF becomeFirstResponder];
        
        return YES;
        
    }else if (textField == _cityTF) {
        
        [_countryTF becomeFirstResponder];
        
        return YES;
        
    }else if (textField == _countryTF) {
        
        [_countryTF resignFirstResponder];
        
        return YES;
        
    }
    
    return YES;
}

#pragma mark - tableview methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    return ArrForAddresses.count;
    
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    cell= [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    
    NSAttributedString *attrStr = [[NSAttributedString alloc] initWithData:[[NSString stringWithFormat:@"%@",[NSString stringWithFormat:@"%@",ArrForAddresses[indexPath.row]]] dataUsingEncoding:NSUnicodeStringEncoding] options:@{ NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType } documentAttributes:nil error:nil];
    
    cell.AddressLabel.attributedText = attrStr;
    
    if ([ArrForSelection[indexPath.row] isEqualToString:@"0"]) {
       // [ArrForSelection replaceObjectAtIndex:indexPath.row withObject:@"1"];
        cell.accessoryType = UITableViewCellAccessoryNone;
    }else{
        cell.accessoryType = UITableViewCellAccessoryCheckmark;
        //[ArrForSelection replaceObjectAtIndex:indexPath.row withObject:@"0"];
    }
    
   // cell.AddressLabel.text = [NSString stringWithFormat:@"%@",ArrForAddresses[indexPath.row]];
    return cell;

}
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath{
    return UITableViewAutomaticDimension;

}
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    ArrForSelection = [[NSMutableArray alloc] init];
    string = @"No";
    for (int a = 0; a<ArrForAddresses.count; a++) {
    
        if (a == indexPath.row) {
            
//            NSLog(@"indecpath %ld",(long)indexPath.row);
//            //[ArrForSelection replaceObjectAtIndex:indexPath.row withObject:@"1"];
//            if ([ArrForSelection[indexPath.row] isEqualToString:@"0"]) {
            
                [ArrForSelection addObject:@"1"];
//            }else{
//                
//                [ArrForSelection replaceObjectAtIndex:indexPath.row withObject:@"0"];
//            }
        }else{
            [ArrForSelection addObject:@"0"];
        
        }
    }
    
    
    [[NSUserDefaults standardUserDefaults]setObject:[ArrForAddresses objectAtIndex:indexPath.row] forKey:@"addressString"];
    
    NSLog(@"ArrForSelection %@",ArrForSelection);
    index = (int)indexPath.row;
    [_savedAddressTV reloadData];
    //cell.accessoryType = UITableViewCellAccessoryCheckmark;
    
    
}

-(void)paymentWithSavedAddress{
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];

    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
//http://voliveafrica.com/noura_services/services/add_saved_address_to_cart
//cust_id,cart_id,store_id,address_id
    
    NSString *strPost = [NSString stringWithFormat:@"cust_id=%@&cart_id=%@&store_id=%@&address_id=%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"],[[NSUserDefaults standardUserDefaults] objectForKey:@"cartId"],languageStr,ArrForaddress[index]];
    [objForSharedClass postMethodForServicesRequest:@"add_saved_address_to_cart" :strPost completion:^(NSDictionary *dic, NSError *err) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
            
            [SVProgressHUD dismiss];
            
        });
        NSLog(@"add_saved_address_to_cart %@",dic);
        
        NSString *str = [NSString stringWithFormat:@"%@",[dic valueForKey:@"status"]];
        if ([str isEqualToString:@"1"]) {
            
        
        label1Str=[[dic valueForKey:@"payment"]valueForKey:@"label"];
        
        value1Str=[[dic valueForKey:@"payment"]valueForKey:@"value"];
        
        label2Str=[[dic valueForKey:@"shipping"]valueForKey:@"label"];
        value2Str=[[dic valueForKey:@"shipping"]valueForKey:@"value"];
        NSLog(@"value2Str %@",value2Str);
        
        totalPriceStr=[dic valueForKey:@"total"];
        
        //NSLog(@"mylable value is %@",[[statusDict objectForKey:@"payment"]objectForKey:@"label"]) ;
        
        [[NSUserDefaults standardUserDefaults] setObject:label1Str forKey:@"label1Str"];
        [[NSUserDefaults standardUserDefaults] setObject:value1Str forKey:@"value1Str"];
        [[NSUserDefaults standardUserDefaults] setObject:label2Str forKey:@"label2Str"];
        [[NSUserDefaults standardUserDefaults] setObject:value2Str forKey:@"value2Str"];
        [[NSUserDefaults standardUserDefaults] setObject:totalPriceStr forKey:@"totalPriceStr"];
            dispatch_async(dispatch_get_main_queue(), ^{

        [self paymentBtnClicked];
            });

        }
    }];
    
}

#pragma mark - Get saved addresses

-(void)GetSavedAddress{
    
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    //http://voliveafrica.com/noura_services/services/saved_addresess?user_id=39&store_id=1
    
    ArrForAddresses = [[NSMutableArray alloc] init];
    ArrForSelection = [[NSMutableArray alloc] init];
    ArrForcompany = [[NSMutableArray alloc] init];
    ArrForfirstName = [[NSMutableArray alloc] init];
    ArrForlastName = [[NSMutableArray alloc] init];
    ArrForAddresses = [[NSMutableArray alloc] init];
    ArrForaddress = [[NSMutableArray alloc] init];
    ArrForcity = [[NSMutableArray alloc] init];
    ArrForstate = [[NSMutableArray alloc] init];
    ArrForphone = [[NSMutableArray alloc] init];
    ArrForpincode = [[NSMutableArray alloc] init];
    ArrForcountry = [[NSMutableArray alloc] init];
    
    
       NSString *url = [NSString stringWithFormat:@"%@saved_addresess?user_id=%@&store_id=%@",base_URL,[[NSUserDefaults standardUserDefaults]objectForKey:@"customerId"],languageStr];
    [objForSharedClass GETList:url completion:^(NSDictionary *dic, NSError *err) {
        
        NSLog(@"saved_addresess %@",dic);
        NSArray *address = [dic valueForKey:@"data"];
        
        for (int i = 0 ; i<address.count; i++) {
            NSDictionary *addDic = address[i];
            
            [ArrForcompany addObject:[addDic valueForKey:@"company"]];
            [ArrForfirstName addObject:[addDic valueForKey:@"first_name"]];
            [ArrForlastName  addObject:[addDic valueForKey:@"last_name"]];
            //[ArrForAddresses addObject:[addDic valueForKey:@"first_name"]];
            [ArrForaddress addObject:[addDic valueForKey:@"id"]];
            [ArrForcity addObject:[addDic valueForKey:@"city"]];
            [ArrForstate addObject:[addDic valueForKey:@"first_name"]];
            [ArrForphone addObject:[addDic valueForKey:@"telephone"]];
            [ArrForpincode addObject:[addDic valueForKey:@"pincode"]];
            [ArrForcountry addObject:[addDic valueForKey:@"country"]];

            
            [ArrForAddresses addObject:[NSString stringWithFormat:@"%@,%@,%@,%@",[addDic valueForKey:@"first_name"],[addDic valueForKey:@"street"],[addDic valueForKey:@"pincode"],[addDic valueForKey:@"telephone"]]];
            [ArrForSelection addObject:@"0"];
            
        }
        dispatch_async(dispatch_get_main_queue(), ^{

        [_savedAddressTV reloadData];
        });

        NSLog(@"ArrForAddresses %@",ArrForAddresses);
        
    }];

}

//***Country Picker View***//
-(void)show
{
    //    countryPickerView.hidden = YES;
    //    toolbar1.hidden = YES;
    if(selected_Type.length == 0)
    {
        _countryTF.text = [countryArr objectAtIndex:0];
        
    }  else
    {
        _countryTF.text = selected_Type;
    }
    
    NSLog(@"myTextValue *** %@",_countryTF.text);
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    
    [_countryTF resignFirstResponder];
    
}
-(void)cancel
{
    [_countryTF resignFirstResponder];
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    
}

//***Country Code Picker View***//
-(void)show1
{
    [_phoneTF becomeFirstResponder];
    //    countryPickerView.hidden = YES;
    //    toolbar1.hidden = YES;
    if(selected_Type1.length == 0)
    {
        _countryCode_TF.text = [countryCodeArray objectAtIndex:0];
        
    }  else
    {
        _countryCode_TF.text = selected_Type1;
    }
    
    NSLog(@"myTextValue *** %@",_countryCode_TF.text);
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    
    [_countryCode_TF resignFirstResponder];
    
}
-(void)cancel1
{
    [_countryCode_TF resignFirstResponder];
    [self.scrollView setContentOffset:CGPointZero animated:YES];
    
}




-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    if (pickerView == countryPickerView) {
        return 1;
    }else if (pickerView == countryCodePickerView){
        return 1;
    }
    
    return 0;
}

// #4
-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    if (pickerView == countryPickerView) {
        return [countryArr count];
    }
    else if (pickerView == countryCodePickerView)
    {
        return [countryCodeArray count];
    }
    
    return 0;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    if (pickerView == countryPickerView) {
        selected_Type = countryArr[row];
    }else if (pickerView == countryCodePickerView)
    {
        selected_Type1 = countryCodeArray[row];
    }
    
}
- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    
    if (pickerView == countryPickerView) {
        return countryArr[row];
    }else if (pickerView == countryCodePickerView){
        return countryCodeArray[row];
    }
    
    return nil;
    
}

-(void)shippingBtnClicked{
    self.shippingView.hidden=NO;
    
    confirmation.view.hidden = YES;
    payment.view.hidden = YES;
    [payment.view removeFromSuperview];
    
    //bottom line
    self.shippingBtmView.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1];
    self.paymentBtmView.backgroundColor = [UIColor whiteColor];
    self.confirmationBtmView.backgroundColor = [UIColor whiteColor];
    
    //label
    self.lblShipping.textColor =[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1];
    self.lblPayment.textColor = [UIColor blackColor];
    self.lblConfirmation.textColor = [UIColor blackColor];
    //image
    self.imgShipping.image = [UIImage imageNamed:@"1"];
    self.imgPayment.image = [UIImage imageNamed:@"22"];
    self.imgConfirmation.image = [UIImage imageNamed:@"33"];
}
-(void)paymentBtnClicked{
    
    
    dispatch_async(dispatch_get_main_queue(), ^{
    
    payment.view.hidden = NO;

    [payment.continueBtn addTarget:self action:@selector(continueConfirmationClicked) forControlEvents:UIControlEventTouchUpInside];
   
    self.shippingView.hidden=YES;
    confirmation.view.hidden = YES;
    payment.view.frame = CGRectMake(0, 0, width, height-150);

    [self.scrollView addSubview:payment.view];
    //bottom line
    self.paymentBtmView.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1];
    self.shippingBtmView.backgroundColor = [UIColor whiteColor];
    self.confirmationBtmView.backgroundColor = [UIColor whiteColor];
    
    //label
    self.lblPayment.textColor =[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1];
    self.lblShipping.textColor = [UIColor blackColor];
    self.lblConfirmation.textColor = [UIColor blackColor];
    //image
    self.imgShipping.image = [UIImage imageNamed:@"11"];
    self.imgPayment.image = [UIImage imageNamed:@"2"];
    self.imgConfirmation.image = [UIImage imageNamed:@"33"];
    });
    
}
-(void)confirmationBtnClicked{
    
    dispatch_async(dispatch_get_main_queue(), ^{

    confirmation.view.hidden = NO;

    [confirmation.placeBtn addTarget:self action:@selector(placeOrderBtnClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    self.shippingView.hidden=YES;
    payment.view.hidden = YES;
    confirmation.view.frame = CGRectMake(0, 0, width, height+20);
    
    [self.scrollView setContentSize:CGSizeMake(width,height+100)];

    [self.scrollView addSubview:confirmation.view];
    
    
    //bottom line

    self.confirmationBtmView.backgroundColor = [UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1];
    self.shippingBtmView.backgroundColor = [UIColor whiteColor];
    self.paymentBtmView.backgroundColor = [UIColor whiteColor];
    
    //label
    self.lblConfirmation.textColor =[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1];
    self.lblPayment.textColor = [UIColor blackColor];
    self.lblShipping.textColor = [UIColor blackColor];
    //image
    self.imgShipping.image = [UIImage imageNamed:@"11"];
    self.imgPayment.image = [UIImage imageNamed:@"22"];
    self.imgConfirmation.image = [UIImage imageNamed:@"3"];
    });

}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
-(void)placeOrderBtnClicked{
    
    ThankYouOrderViewController *thank = [self.storyboard instantiateViewControllerWithIdentifier:@"ThankYouOrderViewController"];
    
    [self.tabBarController.navigationController pushViewController:thank animated:YES];
}
/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (void)addCartAddressServiceCall {
    [_firstName resignFirstResponder];
    [_lastNameTF resignFirstResponder];
    [_countryCode_TF resignFirstResponder];
    [_phoneTF resignFirstResponder];
    [_addressTF resignFirstResponder];
    [_cityTF resignFirstResponder];
    [_countryTF resignFirstResponder];
 
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];

    
    
    
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
   // http://voliveafrica.com/noura_services/services/add_address_to_cart?cust_id=81&company=volive&cart_id=246&fname=sai&lname=vamshi&address=hyd&city=hyd&state=telangana&mobile=1234567890&pincode=500049&country=SA&store_id=1
    
    NSString *url = @"http://voliveafrica.com/makeup_services/services/add_address_to_cart";
    
    NSMutableDictionary * addAddressPostDictionary = [[NSMutableDictionary alloc]init];
    
    [addAddressPostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"customerId"] forKey:@"cust_id"];
    //[addAddressPostDictionary setObject:@"" forKey:@"company"];
    [addAddressPostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"cartId"] forKey:@"cart_id"];
    
    [addAddressPostDictionary setObject:_firstName.text forKey:@"fname"];
    [addAddressPostDictionary setObject:_lastNameTF.text forKey:@"lname"];
    [addAddressPostDictionary setObject:_addressTF.text forKey:@"address"];
    [addAddressPostDictionary setObject:_cityTF.text forKey:@"city"];
    [addAddressPostDictionary setObject:_phoneTF.text forKey:@"mobile"];
    combinedString = [NSString stringWithFormat:@"%@-%@",_countryCode_TF.text,_phoneTF.text];
    //combinedString = [_countryCode_TF.text stringByAppendingString:_phoneTF.text];
    [addAddressPostDictionary setObject:combinedString forKey:@"mobile"];
    [addAddressPostDictionary setObject:_countryTF.text forKey:@"country"];
    [addAddressPostDictionary setObject:defaultString forKey:@"is_save"];
    [addAddressPostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"emailId"] forKey:@"email"];
    //[addAddressPostDictionary setObject:@"1" forKey:@"store_id"];
  
    [addAddressPostDictionary setObject:languageStr forKey:@"store_id"];
    
    NSLog(@"addAddressPostDictionary %@",addAddressPostDictionary);
    
    NSMutableURLRequest *request=[NSMutableURLRequest requestWithURL:[NSURL URLWithString:url]];
    
    NSString *BoundaryConstant = @"----------V2ymHFg03ehbqgZCaKO6jy";
    
    // string constant for the post parameter 'file'. My server uses this name: `file`. Your's may differ
    
    [request setCachePolicy:NSURLRequestReloadIgnoringLocalCacheData];
    [request setHTTPShouldHandleCookies:NO];
    [request setTimeoutInterval:30];
    [request setHTTPMethod:@"POST"];
    
    // set Content-Type in HTTP header
    NSString *contentType = [NSString stringWithFormat:@"multipart/form-data; boundary=%@", BoundaryConstant];
    [request setValue:contentType forHTTPHeaderField: @"Content-Type"];
    
    // post body
    NSMutableData *body = [NSMutableData data];
    
    // add params (all params are strings)
    for (NSString *param in addAddressPostDictionary) {
        [body appendData:[[NSString stringWithFormat:@"--%@\r\n", BoundaryConstant] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"Content-Disposition: form-data; name=\"%@\"\r\n\r\n", param] dataUsingEncoding:NSUTF8StringEncoding]];
        [body appendData:[[NSString stringWithFormat:@"%@\r\n", [addAddressPostDictionary objectForKey:param]] dataUsingEncoding:NSUTF8StringEncoding]];
        [request setHTTPBody:body];
    }
    
    
    
    
    // set the content-length
    NSString *postLength = [NSString stringWithFormat:@"%lu", (unsigned long)[body length]];
    [request setValue:postLength forHTTPHeaderField:@"Content-Length"];
    
    NSURLSession *session = [NSURLSession sharedSession];
    NSLog(@"The request is %@",request);
    
    [[session dataTaskWithRequest:request completionHandler:^(NSData *  _Nullable data, NSURLResponse *_Nullable response, NSError * _Nullable error) {
        dispatch_async(dispatch_get_main_queue(), ^{
            
           
               // [objForSharedClass.hud hideAnimated:YES];
            
        });
        statusDict = [[NSDictionary alloc]init];
        statusDict = [NSJSONSerialization JSONObjectWithData:data options:0 error:nil];
        
        NSLog(@"Images from server %@", statusDict);
        
        NSString * status1 = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"status"]];
        //NSString * message1 = [NSString stringWithFormat:@"%@",[statusDict objectForKey:@"message"]];
        if ([status1 isEqualToString:@"1"]) {
            
            [SVProgressHUD dismiss];
           NSString * addressString=[NSString stringWithFormat:@"%@ ,%@, %@, %@, %@, %@, %@,%@,%@",self.firstName.text,self.lastNameTF.text,self.phoneTF.text ,self.companyTF.text,self.addressTF.text,self.cityTF.text,self.pincodeTF.text,self.stateTF.text,self.countryTF.text];
            
            [[NSUserDefaults standardUserDefaults]setObject:addressString forKey:@"addressString"];
            
            
            label1Str=[[statusDict objectForKey:@"payment"]objectForKey:@"label"];
            
            value1Str=[[statusDict objectForKey:@"payment"]objectForKey:@"value"];
            
            label2Str=[[statusDict objectForKey:@"shipping"]objectForKey:@"label"];
             value2Str=[[statusDict objectForKey:@"shipping"]objectForKey:@"value"];
            NSLog(@"value2Str %@",value2Str);
            
            totalPriceStr=[statusDict objectForKey:@"total"];
            
            //NSLog(@"mylable value is %@",[[statusDict objectForKey:@"payment"]objectForKey:@"label"]) ;
            
             [[NSUserDefaults standardUserDefaults] setObject:label1Str forKey:@"label1Str"];
             [[NSUserDefaults standardUserDefaults] setObject:value1Str forKey:@"value1Str"];
             [[NSUserDefaults standardUserDefaults] setObject:label2Str forKey:@"label2Str"];
             [[NSUserDefaults standardUserDefaults] setObject:value2Str forKey:@"value2Str"];
             [[NSUserDefaults standardUserDefaults] setObject:totalPriceStr forKey:@"totalPriceStr"];
            
            dispatch_async(dispatch_get_main_queue(), ^{

           [self paymentBtnClicked];
            });
            
        }
        else {
            dispatch_async(dispatch_get_main_queue(), ^{
                [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Alert!"] withMessage:[statusDict objectForKey:@"message"] onViewController:self completion:^{  [self.firstName becomeFirstResponder]; }];
            });
            
        }
        
    }]
     
     resume];
    
}




-(void)continueConfirmationClicked{
    
    [self confirmationBtnClicked];
    
}

- (IBAction)backBtnClicked:(id)sender {
     [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)continuePayBtn:(id)sender {
   // [objForSharedClass alertforLoading:self];
    
    if (self.cityTF.text.length==0) {
        
    }
    if (_segmentController.selectedSegmentIndex == 0) {
        
        
        if (self.firstName.text.length < 3 ) {
            
            [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Please enter a valid name"] onViewController:self completion:^{  [self.firstName becomeFirstResponder]; }];
            
        } if (self.lastNameTF.text.length == 0 ) {
            
            [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Please enter Lastname"] onViewController:self completion:^{  [self.lastNameTF becomeFirstResponder]; }];
            
        }  else if (self.phoneTF.text.length < 5 ) {
            
            [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Mobile number should be more than 5 digits"] onViewController:self completion:^{  [self.phoneTF becomeFirstResponder]; }];
            
        }else if (self.cityTF.text.length < 3 ) {
            
            [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Please enter a valid city name"] onViewController:self completion:^{  [self.cityTF becomeFirstResponder]; }];
            
        }else if (self.countryTF.text.length == 0 ) {
            
            [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Please select a country"] onViewController:self completion:^{  [self.countryTF becomeFirstResponder]; }];
            
        } else {
            
            [self addCartAddressServiceCall];
            
        }
        
    }else{
        if ([string  isEqualToString:@"No"] ) {
            [self paymentWithSavedAddress];
        }
        else
        {
            UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"]
                                        
                                                                           message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Please select an address"] preferredStyle:UIAlertControllerStyleAlert];
            
            UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                               style:UIAlertActionStyleCancel
                                                             handler:^(UIAlertAction * _Nonnull action) {
                                                                 
                                                             }];
            [alert addAction:okButton];
            [self presentViewController:alert animated:YES completion:nil];

        }
       
    
    }
    
    
    
   }
- (IBAction)segmentAction:(id)sender {
    
    if(_segmentController.selectedSegmentIndex==0){
       _savedAddressTV.hidden=YES;
        _AddressView.hidden=NO;
        
        
    }
    else if(_segmentController.selectedSegmentIndex==1){
        
        [_savedAddressTV reloadData];
        _savedAddressTV.hidden=NO;
        _AddressView.hidden=YES;
        
    }
    
    
    
}
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
    if (textField == _firstName) {
        
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-20) animated:YES];
        
        
        
    }else if (textField == _lastNameTF) {
        
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-10) animated:YES];
        
    }else if (textField == _phoneTF) {
        
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-10) animated:YES];
        
    }else if (textField == _companyTF) {
        
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-10) animated:YES];
        
    }else if (textField == _addressTF) {
        
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-10) animated:YES];
        
    }else if (textField == _cityTF) {
        
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-10) animated:YES];
        
    }else if (textField == _pincodeTF) {
        
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-10) animated:YES];
        
    }else if (textField == _countryTF) {
        
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-10) animated:YES];
        
    }
    
}



- (IBAction)saveButon:(id)sender {
    isChecked=!isChecked;
    if (isChecked == YES) {
        _saveImageView.image=[UIImage imageNamed:@"check"];
        defaultString = [NSString stringWithFormat:@"True"];
        
    }else
    {
        _saveImageView.image=[UIImage imageNamed:@"checkbox"];
        defaultString = [NSString stringWithFormat:@"False"];
    }

}
@end
