//
//  WishListTableViewCell.m
//  Noura App
//
//  Created by volive solutions on 22/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "WishListTableViewCell.h"
#import "SharedClass.h"

@implementation WishListTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _addToCartBtn.layer.masksToBounds = false;
    _addToCartBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _addToCartBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _addToCartBtn.layer.shadowRadius = 2;
    _addToCartBtn.layer.shadowOpacity = 0.5;
    _quantityLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Quantity :"];
    [_addToCartBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"ADD TO CART"] forState:UIControlStateNormal];

    
    self.quantityTF.delegate=self;
    
        UITapGestureRecognizer * tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textFieldDidEndEditing:)];
        [self.contentView addGestureRecognizer:tap];
   
 
}

//- (BOOL)textFieldShouldReturn:(UITextField *)textField
//{
//    
//    [self.quantityTF resignFirstResponder];
//    
//    
//    return YES;
//    
//}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
      [self.quantityTF resignFirstResponder];
}



- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)addToCartButton:(id)sender {

}

- (IBAction)deleteButton:(id)sender {

}
- (IBAction)stepperClicked:(id)sender {
}
@end
