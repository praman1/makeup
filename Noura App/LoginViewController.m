//
//  ViewController.m
//  Noura App
//
//  Created by volive solutions on 18/08/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "LoginViewController.h"
#import "RegisterViewController.h"
#import "HomeViewController.h"
#import "SWRevealViewController.h"
#import "LineTextField.h"
#import "SharedClass.h"
#import "SVProgressHUD.h"
#import "LineTextField.h"
#import "AppDelegate.h"
@interface LoginViewController ()
{
    BOOL isChecked;
    NSString * emailString;
    NSString * languageString,*cartItems;
    SharedClass *objForSharedClass;
     UIAlertController *alertController;
    AppDelegate * menuAppDelegate;
    //NSString *selectedLanguage;
}

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    languageString = [NSString stringWithFormat:@"2"];
    
    [[NSUserDefaults standardUserDefaults]setInteger:[languageString integerValue] forKey:@"language"];
    
    [self loadViewWithCustomDesign];
    
    //[[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(reloadDataForBadge) name:@"refreshData" object:nil];
    
}

//-(void)reloadDataForBadge
//{
//    [self loginServiceCall];
//}

-(void)viewWillAppear:(BOOL)animated{
    dispatch_async(dispatch_get_main_queue(), ^{
        
        [self.navigationController setNavigationBarHidden:YES animated:YES];   //it hides
        
        [UIView animateWithDuration:0 animations:^{
            
            self.logoImageView.transform = CGAffineTransformMakeScale(0.01, 0.01);
            
        }completion:^(BOOL finished){
            
            // Finished scaling down imageview, now resize it
            
            [UIView animateWithDuration:0.4 animations:^{
                
                self.logoImageView.transform = CGAffineTransformIdentity;
                
            }completion:^(BOOL finished){
                
                
            }];
            
            
        }];
        
    });

}

-(void)loadViewWithCustomDesign {
    
    //_logoImageView.image = [UIImage imageNamed:@"New Logo(En)"];
    
    objForSharedClass = [[SharedClass alloc] init];
    isChecked = NO;
    _usernameTF.delegate = self;
    _passwordTF.delegate = self;
    LineTextField *line = [[LineTextField alloc]init];
    
    [line textfieldAsLine:_usernameTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Email Address"] myView:self.view];
    [line textfieldAsLine:_passwordTF lineColor:[UIColor lightGrayColor] placeHolder:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Password"] myView:self.view];

    UITapGestureRecognizer * tap =[[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(textFieldDidEndEditing:)];
    [_loginView addGestureRecognizer:tap];
    [_scrollView addGestureRecognizer:tap];

    
    int selectedLanguage = (int)[[NSUserDefaults standardUserDefaults] integerForKey:@"language"];
    
    if(selectedLanguage == 2){
        
        _usernameTF.textAlignment = NSTextAlignmentRight;
        _passwordTF.textAlignment = NSTextAlignmentRight;
    
    } else  {
        
        _usernameTF.textAlignment = NSTextAlignmentLeft;
        _passwordTF.textAlignment = NSTextAlignmentLeft;
    }
    
    
    _rememberMeLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Remember me"];
    
    [_forgotPasswordBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Forgot Password ?"] forState:UIControlStateNormal];
    
   // _usernameTF.layer.cornerRadius = 8;
   // _usernameTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Username"];
    
   // _passwordTF.layer.cornerRadius = 8;
    //_passwordTF.placeholder = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Password"];
    
//    _usernameTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
//    _passwordTF.layer.borderColor = [UIColor lightGrayColor].CGColor;
    
//    _usernameTF.layer.borderWidth = 1.2f;
//    _passwordTF.layer.borderWidth = 1.2f;
    
    _loginBtn.layer.cornerRadius = 22;
    [_loginBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Login"] forState:UIControlStateNormal];
    
    _signUpBtn.layer.cornerRadius = 23;
    [_signUpBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Sign Up With Email"] forState:UIControlStateNormal];
    _signUpBtn.layer.borderColor = [[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]CGColor];
    _signUpBtn.layer.borderWidth = 1.2f;
    [self.navigationController setNavigationBarHidden:YES];
    
    _usernameTF.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"saveEmail"];
    _passwordTF.text = [[NSUserDefaults standardUserDefaults]objectForKey:@"savePassword"];
    
    [_changeLanguageBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Change Language"] forState:UIControlStateNormal];
    _loginBtn.layer.masksToBounds = false;
    _loginBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _loginBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _loginBtn.layer.shadowRadius = 3;
    _loginBtn.layer.shadowOpacity = 0.5;
    
    _changeLanguageBtn.layer.masksToBounds = false;
    _changeLanguageBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _changeLanguageBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _changeLanguageBtn.layer.shadowRadius = 3;
    _changeLanguageBtn.layer.shadowOpacity = 0.5;

    _signUpBtn.layer.masksToBounds = false;
    _signUpBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _signUpBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _signUpBtn.layer.shadowRadius = 3;
    _signUpBtn.layer.shadowOpacity = 0.5;
    
    // Do any additional setup after loading the view, typically from a nib.
    

    
}


#pragma mark - textfield delegate methods
- (void)textFieldDidBeginEditing:(UITextField *)textField
{
     if (textField == _usernameTF) {
        
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-80) animated:YES];
         
    }else if (textField == _passwordTF) {
        
        [_scrollView setContentOffset:CGPointMake(0, textField.frame.origin.y-120) animated:YES];
        
    }
}

- (void)textFieldDidEndEditing:(UITextField *)textField

{
//    if (textField==_usernameTF) {
//        NSString *emailid = _usernameTF.text;
//        NSString *emailCharacters = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}";
//        NSPredicate *emailTest =[NSPredicate predicateWithFormat:@"SELF MATCHES %@",emailCharacters];
//        
//        if([emailTest evaluateWithObject:emailid]){
//            
//        }
//        else{
//            //_usernameTF.text=@"";
//            [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Please enter valid email id"] onViewController:self completion:^{  [self.usernameTF becomeFirstResponder];
//                [_scrollView setContentOffset:CGPointMake(0,0) animated:YES];
//            }];
//            
//        }
    
    [_usernameTF resignFirstResponder];
    [_passwordTF resignFirstResponder];
    
    [_scrollView setContentOffset:CGPointMake(0,0) animated:YES];
    
//}
}

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    
    BOOL returnValue = NO;
    
    if (textField == _usernameTF) {
        
        [_passwordTF becomeFirstResponder];
        
        returnValue = YES;
        
    }else if (textField == _passwordTF) {
        
        [_passwordTF resignFirstResponder];
        
        returnValue = YES;
        
    }
    
    return returnValue;
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)forgotPasswordButton:(id)sender {
   /* ForgotPasswordViewController *forgot = [self.storyboard instantiateViewControllerWithIdentifier:@"ForgotPasswordViewController"];
    [self.navigationController pushViewController:forgot animated:YES];*/
    
     alertController = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert !"]
                                                                              message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Enter your registered email id"]
                                                                       preferredStyle:UIAlertControllerStyleAlert];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField) {
        textField.placeholder =[[SharedClass sharedInstance]languageSelectedStringForKey:@"Enter Email Id"];
        textField.textColor = [UIColor blackColor];
        textField.clearButtonMode = UITextFieldViewModeWhileEditing;
        textField.borderStyle = UITextBorderStyleRoundedRect;
        textField.keyboardType=UIKeyboardTypeEmailAddress;
    }];
   
    [alertController addAction:[UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"] style:UIAlertActionStyleDefault handler:^(UIAlertAction *action) {
       
        NSArray * textfields = alertController.textFields;
        
        UITextField * namefield = textfields[0];
        
        //_emailID.text = emailString;
        NSLog(@"jfhgjdjgd %@",namefield);
        
        emailString=namefield.text;
        
      //  // http://voliveafrica.com/makeup_services/services/forget_password?email=rajgopal@volivesolutions.com
        
            NSMutableDictionary * forgotPWPostDictionary = [[NSMutableDictionary alloc]init];
        
            [forgotPWPostDictionary setObject:emailString forKey:@"email"];
        
            //[[SharedClass sharedInstance]showprogressfor:@"Loading.."];
        
            [[SharedClass sharedInstance]fetchResponseforParameter:@"forget_password?" withPostDict:forgotPWPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
                NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
                NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
               // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
                NSLog(@"%@",dataFromJson);
                NSLog(@"%@",dataDictionary);
                [SVProgressHUD dismiss];
                if ([status isEqualToString:@"1"])
                {
        
        
                    dispatch_async(dispatch_get_main_queue(), ^{
        
                        NSLog(@"successfully sent to emailid ");
                        
                        UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Success"]
                                                    
                                                                                       message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Your password is sent to your emailid. Please reset the password"] preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                           style:UIAlertActionStyleDefault
                                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                                             
                                                                         }];
                        [alert addAction:okButton];
                        [self presentViewController:alert animated:YES completion:nil];

        
                    });
                                   } else {
        
                    dispatch_async(dispatch_get_main_queue(), ^{
        
                        //[SVProgressHUD dismiss];
//                        [[SharedClass sharedInstance]showAlertWithTitle:@"Alert!" withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
                        
                        
 UIAlertController *alert = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"]  message:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Please enter a valid emailid"] preferredStyle:UIAlertControllerStyleAlert];
                        
                        UIAlertAction *okButton = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Ok"]
                                                                           style:UIAlertActionStyleDefault
                                                                         handler:^(UIAlertAction * _Nonnull action) {
                                                                             
                                                                         }];
                        [alert addAction:okButton];
                        [self presentViewController:alert animated:YES completion:nil];
          
                        
                    });
                    
                }
                
            }];
            
        
    }]];
   
    [self presentViewController:alertController animated:YES completion:nil];


}

- (IBAction)rememberMeButton:(id)sender {
    isChecked=!isChecked;
    if (isChecked == YES) {
        _rememberMeCheckBox.image = [UIImage imageNamed:@"check"];
        
    }else if (isChecked == NO)
    {
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"saveEmail"];
        [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"savePassword"];
        _rememberMeCheckBox.image = [UIImage imageNamed:@"checkbox"];
    }
}
-(void)alert :(NSString *)Msg{
    // UIAlertController *controller = [];
    
    alertController = [UIAlertController alertControllerWithTitle:@"" message:Msg preferredStyle:UIAlertControllerStyleAlert];
    
    //UIAlertAction* ok = [UIAlertAction actionWithTitle:@"OK" style:UIAlertActionStyleDefault handler:nil];
    //[alertController addAction:ok];
    
    [self presentViewController:alertController animated:YES completion:nil];
}


-(void)loginServiceCall
{
    //  http://voliveafrica.com/makeup_services/services/login?email=raj@gmail.com&password=123456
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    
    
    NSMutableDictionary * loginPostDictionary = [[NSMutableDictionary alloc]init];
    
    [loginPostDictionary setObject:self.usernameTF.text forKey:@"email"];
    [loginPostDictionary setObject:self.passwordTF.text forKey:@"password"];
    //[[SharedClass sharedInstance]showprogressfor:@"Loading.."];
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"login?" withPostDict:loginPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        //NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@",dataDictionary);
        [SVProgressHUD dismiss];
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                [[NSUserDefaults standardUserDefaults]setObject:[[dataDictionary objectForKey:@"data"]objectForKey:@"store_id"]forKey:@"saveStore"];
                [[NSUserDefaults standardUserDefaults]setObject:[[dataDictionary objectForKey:@"data"]objectForKey:@"cust_id"]forKey:@"customerId"];
                [[NSUserDefaults standardUserDefaults]setObject:[[dataDictionary objectForKey:@"data"]objectForKey:@"email"]forKey:@"emailId"];
                [[NSUserDefaults standardUserDefaults]setObject:[[dataDictionary objectForKey:@"data"]objectForKey:@"firstname"]forKey:@"name"];
                [[NSUserDefaults standardUserDefaults]setObject:[[dataDictionary objectForKey:@"data"]objectForKey:@"cart_id"]forKey:@"cartId"];
                [[NSUserDefaults standardUserDefaults]setObject:[[dataDictionary objectForKey:@"data"]objectForKey:@"mobile"]forKey:@"mobileNumber"];
//                cartItems =[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"total_cart_items"] ];
                //[[NSUserDefaults standardUserDefaults]setObject:cartItems forKey:@"cartItemsLogin"];
                //[[[NSUserDefaults standardUserDefaults]objectForKey:@"total_cart_items"]objectForKey:@"cartItemsLogin"];
            });
           // [[NSNotificationCenter defaultCenter]postNotificationName:@"refreshData" object:nil];
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                if(isChecked == YES)
                {
                    
                    [[NSUserDefaults standardUserDefaults]setObject:self.usernameTF.text forKey:@"saveEmail"];
                    [[NSUserDefaults standardUserDefaults]setObject:self.passwordTF.text forKey:@"savePassword"];
                }
                else{
                    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"saveEmail"];
                    [[NSUserDefaults standardUserDefaults]setObject:@"" forKey:@"savePassword"];
                }
                
               // [objForSharedClass.hud hideAnimated:YES];
                
                menuAppDelegate.selectTabItem = @"0";
                SWRevealViewController*reveal=[self.storyboard instantiateViewControllerWithIdentifier:@"SWRevealViewController"];
                
                [self presentViewController:reveal animated:YES completion:^{    }];
                
               // [self cartBadgeValueServiceCall];
                
            });
            
            
        } else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                //[objForSharedClass.hud hideAnimated:YES];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[SharedClass sharedInstance]languageSelectedStringForKey:[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
             });
            
        }
        
    }];
    
    
}
    
//-(void)cartBadgeValueServiceCall
//{
//    //http://voliveafrica.com/makeup_services/services/total_cart_items?customer_id=31&store_id=1
//    NSMutableDictionary * cartBadgePostDictionary = [[NSMutableDictionary alloc]init];
//    
//    [cartBadgePostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"customerId"]forKey:@"customer_id"];
//    [cartBadgePostDictionary setObject:[[NSUserDefaults standardUserDefaults] objectForKey:@"saveStore"]forKey:@"store_id"];
//    //[[SharedClass sharedInstance]showprogressfor:@"Loading.."];
//    
//    [[SharedClass sharedInstance]fetchResponseforParameter:@"total_cart_items?" withPostDict:cartBadgePostDictionary andReturnWith:^(NSData *dataFromJson) {
//        
//        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
//        
//        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
//        //NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
//        NSLog(@"%@",dataFromJson);
//        NSLog(@"%@",dataDictionary);
//        [SVProgressHUD dismiss];
//        if ([status isEqualToString:@"1"])
//        {
//            dispatch_async(dispatch_get_main_queue(), ^{
//                
//                [SVProgressHUD dismiss];
//                [[NSUserDefaults standardUserDefaults]setObject:[dataDictionary objectForKey:@"total"] forKey:@"cartBadge"];
//                NSLog(@"Badge Value Is%@",[[NSUserDefaults standardUserDefaults]objectForKey:@"cartBadge"]);
//                
//                    });
//            
//        } else {
//            
//            dispatch_async(dispatch_get_main_queue(), ^{
//                
//                [SVProgressHUD dismiss];
//                //[objForSharedClass.hud hideAnimated:YES];
//                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[[SharedClass sharedInstance]languageSelectedStringForKey:[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
//            });
//            
//        }
//        
//    }];
//
//}

- (IBAction)logInBtn:(id)sender {
    
    [_usernameTF resignFirstResponder];
    [_passwordTF resignFirstResponder];
    
    
    NSString *emailRegEx = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,10}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegEx];
     if ([emailTest evaluateWithObject:self.usernameTF.text] == NO ) {
        
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Please enter valid email id"] onViewController:self completion:^{  [self.usernameTF becomeFirstResponder]; }];
        
    } else if (self.passwordTF.text.length < 5 ) {
        
        [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Warning...!"] withMessage:[[SharedClass sharedInstance] languageSelectedStringForKey:@"Password should be more than 5 characters"] onViewController:self completion:^{  [self.passwordTF becomeFirstResponder]; }];
        
    }
    else{
        [self loginServiceCall];
    }
}

    
    
    
    
    


- (IBAction)signUpButton:(id)sender {
    RegisterViewController *signUp = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterViewController"];
    [self.navigationController pushViewController:signUp animated:YES];
}
- (IBAction)changeLanguageBtn:(id)sender {
    [self changeLanguage];

}

-(void)changeLanguage
{
    
    alertController = [UIAlertController alertControllerWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Change Language"] message:@"" preferredStyle:UIAlertControllerStyleActionSheet];
    UIAlertAction *english = [UIAlertAction actionWithTitle:@"English" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        languageString = [NSString stringWithFormat:@"1"];
       // _logoImageView.image = [UIImage imageNamed:@"New Logo(En)"];
       

        [[NSUserDefaults standardUserDefaults]setInteger:[languageString integerValue] forKey:@"language"];
         [self loadViewWithCustomDesign];
        
    }];
    UIAlertAction *arabic = [UIAlertAction actionWithTitle:@"العربية" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action) {
        languageString = [NSString stringWithFormat:@"2"];
       // _logoImageView.image = [UIImage imageNamed:@"New Logo(Ar)"];
       
        [[NSUserDefaults standardUserDefaults]setInteger:[languageString integerValue] forKey:@"language"];
         [self loadViewWithCustomDesign];

        
    }];
    
    //UIAlertAction *latest = [UIAlertAction actionWithTitle:@"Latest" style:UIAlertActionStyleDefault handler:nil];
    UIAlertAction *cancel = [UIAlertAction actionWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Cancel"] style:UIAlertActionStyleCancel handler:nil];
    
    [alertController addAction:english];
    [alertController addAction:arabic];
    [alertController addAction:cancel];
    
    [self presentViewController:alertController animated:YES completion:nil];
    
    
}





@end
