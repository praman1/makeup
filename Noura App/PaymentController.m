//
//  PaymentController.m
//  Noura App
//
//  Created by Mohammad Apsar on 8/30/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "PaymentController.h"
#import "CartController.h"
#import "SharedClass.h"

@interface PaymentController ()
{
    NSString * payment_method_str;
    NSString * shipping_method_str;
    NSString *addressString;
    NSString * cust_Id;
    NSString * paymentString;
    BOOL isChecked;
    NSString *CODString,*onlinePayString;
}

@end

@implementation PaymentController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadViewWithCustomDesign];
    
}

-(void)loadViewWithCustomDesign {
    
    isChecked = NO;
    _onlinePaymentLabel.text =[[SharedClass sharedInstance]languageSelectedStringForKey:@"Online Payment"];
    _promoCodeLabel.text =[[SharedClass sharedInstance]languageSelectedStringForKey:@"Promo Code"];
    //_FlatRateLbl.text =[[SharedClass sharedInstance]languageSelectedStringForKey:@"Shipping Fees"];
    [_applyBtn_Outlet setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"APPLY"] forState:UIControlStateNormal];
    _continueConfirmBtn.layer.masksToBounds = false;
    _continueConfirmBtn.layer.shadowColor = [UIColor blackColor].CGColor;
    _continueConfirmBtn.layer.shadowOffset = CGSizeMake(2, 2);
    _continueConfirmBtn.layer.shadowRadius = 3;
    _continueConfirmBtn.layer.shadowOpacity = 0.5;
    
    [_continueConfirmBtn setTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"CONTINUE TO CONFIRAMATION"] forState:UIControlStateNormal];
    
    _totalAmountLabel.text = [[SharedClass sharedInstance]languageSelectedStringForKey:@"Total Amount"];
    
    //_cashOnDeliveryLbl.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"label1Str"];
    _cashOnDeliveryLbl.text=[[SharedClass sharedInstance]languageSelectedStringForKey:@"Cash on delivery"];
    //_FlatRateLbl.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"label2Str"];
    _amountLbl.text=[[NSUserDefaults standardUserDefaults]objectForKey:@"totalPriceStr"];
    
    //_cashOnDeliveryLbl.text=[[SharedClass sharedInstance]languageSelectedStringForKey:@"Cash on delivery"];
   // _FlatRateLbl.text=[[SharedClass sharedInstance]languageSelectedStringForKey:@"Flat Rate"];

    
    
    payment_method_str=[[NSUserDefaults standardUserDefaults]objectForKey:@"value1Str"];
    shipping_method_str=[[NSUserDefaults standardUserDefaults]objectForKey:@"value2Str"];
    NSLog(@"payment_method_str %@  : shipping_method_str %@",payment_method_str,shipping_method_str);
    // [shipping_method_str stringByReplacingOccurrencesOfString:@"""" withString:@""];
    
    
//    [_CODBtn_Outlet addTarget:self action:@selector(CODClicked) forControlEvents:UIControlEventTouchUpInside];
//    [_onlinePayBtn_Outlet addTarget:self action:@selector(onlinePayClicked) forControlEvents:UIControlEventTouchUpInside];
    
    
    
    UITapGestureRecognizer *cashOnDelivery = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(cashOnDeliverytapped)];
    
    [self.cashOnDeliveryView addGestureRecognizer:cashOnDelivery];
    
    UITapGestureRecognizer *flatrate = [[UITapGestureRecognizer alloc]initWithTarget:self action:@selector(flatRateTapped)];
    
    [self.cashOnDeliveryView addGestureRecognizer:flatrate];
    
}


-(void)cashOnDeliverytapped{
    
    paymentString =@"cashOnDelivery";
    
}
-(void)flatRateTapped{
    paymentString =@"flatRate";
}

- (IBAction)continueConfirmBtn:(id)sender {
    
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
   
    // http://voliveafrica.com/noura_services/services/order_confirmation?cart_id=246&shipping_method=flatrate_flatrate&payment_method=cashondelivery&store_id=1
    
    
    NSMutableDictionary *cartProductPostDictionary = [[NSMutableDictionary alloc]init];
    
    [cartProductPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"cartId"] forKey:@"cart_id"];
    [cartProductPostDictionary setObject:shipping_method_str forKey:@"shipping_method"];
    
    [cartProductPostDictionary setObject:payment_method_str forKey:@"payment_method"];
    
    [cartProductPostDictionary setObject:languageStr forKey:@"store_id"];
    //[cartProductPostDictionary setObject:@"1" forKey:@"store_id"];
    
    NSLog(@"cartProductPostDictionary %@",cartProductPostDictionary);
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"order_confirmation?" withPostDict:cartProductPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        //NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"success"]];
        
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                [SVProgressHUD dismiss];
                
            });
            NSLog(@"my data is printing *****");
            
            addressString=[NSString stringWithFormat:@"%@ ,%@, %@, %@, %@, %@",
                           [[dataDictionary objectForKey:@"address"]objectForKey:@"firstname"],
                           [[dataDictionary objectForKey:@"address"]objectForKey:@"lastname"],
                           [[dataDictionary objectForKey:@"address"]objectForKey:@"street"],
                           [[dataDictionary objectForKey:@"address"]objectForKey:@"city"],
                           
                           [[dataDictionary objectForKey:@"address"]objectForKey:@"postcode"],[[dataDictionary objectForKey:@"address"]objectForKey:@"telephone"]];
            
            NSLog(@"myString is %@",addressString);
            
            [[NSUserDefaults standardUserDefaults] setObject:addressString forKey:@"addressString"];
            
            // cust_Id=[[dataDictionary objectForKey:@"address"]objectForKey:@"customer_id"];
            
            
            // [[NSUserDefaults standardUserDefaults] setObject:cust_Id forKey:@"cust_Id"];
            
            
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                 [SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    
   
}

- (IBAction)COD_BTN:(id)sender {
}

- (IBAction)onlinePay_BTN:(id)sender {
}
- (IBAction)promoApply_BTN:(id)sender {
}

- (IBAction)CODCheck_BTN:(id)sender {
    
    
        dispatch_async(dispatch_get_main_queue(), ^{
            [_CODBtn_Outlet setImage:[UIImage imageNamed:@"select radio"] forState:UIControlStateNormal];
            [_onlinePayBtn_Outlet setImage:[UIImage imageNamed:@"unselect radio"] forState:UIControlStateNormal];
        //_CODImageView.image = [UIImage imageNamed:@"select radio"];
       // _onlinePayImageView.image = [UIImage imageNamed:@"unselect radio"];
            });
      CODString = @"Cash On Delivery";

}

- (IBAction)onlinePayCheck_BTN:(id)sender {
    
        dispatch_async(dispatch_get_main_queue(), ^{
            [_onlinePayBtn_Outlet setImage:[UIImage imageNamed:@"select radio"] forState:UIControlStateNormal];
            [_CODBtn_Outlet setImage:[UIImage imageNamed:@"unselect radio"] forState:UIControlStateNormal];
            
       //_onlinePayImageView.image = [UIImage imageNamed:@"select radio"];
       // _CODImageView.image = [UIImage imageNamed:@"unselect radio"];
            
             });
      onlinePayString = @"Online Payment";

}



- (IBAction)apply_BTN:(id)sender {
}
@end
