//
//  LineTextField.h
//  JSON
//
//  Created by Volivesolutions on 11/25/16.
//  Copyright (c) 2016 Volivesolutions. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

@interface LineTextField : NSObject

-(void)textfieldAsLine:(UITextField *)myTextfield
             lineColor:(UIColor *)lineColor
           placeHolder:(NSString *)placeholder
                myView:(UIView *)view;

@end
