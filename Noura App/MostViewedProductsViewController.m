//
//  MostViewedProductsViewController.m
//  Noura App
//
//  Created by volive solutions on 28/09/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "MostViewedProductsViewController.h"

#import "SharedClass.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "HCSStarRatingView.h"
#import "ProductInfoViewController.h"

@interface MostViewedProductsViewController ()
{
    
    NSMutableArray * productsImageArr;
    NSMutableArray *productsNameArr;
    NSMutableArray *productPriceArr;
    NSMutableArray *productOldArr;
    NSMutableArray * ratingArr;
    NSMutableArray *productIdArr;
    
    NSMutableArray *productsArrayCount;
    NSMutableArray * productNamesArrayCount;
    
    
      
}


@end

@implementation MostViewedProductsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadViewWithCustomDesign];
    
    

}

-(void)loadViewWithCustomDesign {
    self.navigationItem.title = [[SharedClass sharedInstance] languageSelectedStringForKey:@"View All"];
    productsImageArr =[NSMutableArray new];
    
    productsNameArr =[NSMutableArray new];
    productPriceArr =[NSMutableArray new];
    productOldArr =[NSMutableArray new];
    ratingArr =[NSMutableArray new];
    productIdArr = [NSMutableArray new];
    
    productsArrayCount = [NSMutableArray new];
    
    [_mostViewedCollectionView reloadData];
    
    [self viewAllServiceCall];
    
}


-(void)viewAllServiceCall
{
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
     NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    NSMutableDictionary * productsPostDictionary = [[NSMutableDictionary alloc]init];
    [productsPostDictionary setObject:languageStr forKey:@"store_id"];
     //[productsPostDictionary setObject:@"1" forKey:@"store_id"];
    
    [[SharedClass sharedInstance]fetchResponseforParameter:@"most_visited?" withPostDict:productsPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
        //NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                // [objForSharedClass.hud hideAnimated:YES];
                //[hud hideAnimated:YES];
                [SVProgressHUD dismiss];
                
            });
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                productNamesArrayCount=[dataDictionary objectForKey:@"data"];
                
                if (productNamesArrayCount.count>0) {
                    for (int i=0; i<productNamesArrayCount.count; i++) {
                        [productsNameArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"name"]];
                        [productsImageArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"image"]];
                        [productPriceArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"price"]];
                        [ratingArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"rating"]];
                        [productIdArr addObject:[[[dataDictionary objectForKey:@"data"]objectAtIndex:i]objectForKey:@"id"]];
                        
//                        NSLog(@" product array is %@",productIdArr);
//                        
//                        NSLog(@"%@",_productNamesArray);
//                        NSLog(@"%@",_productsImagesArray);
                        [_mostViewedCollectionView reloadData];
                    }
                }
                
            });
        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                //[SVProgressHUD dismiss];
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];
    
}


-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    
    return productsNameArr.count;
    
}

- (UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath{
    
    //    if (collectionView == _viewAllCollectionView) {
    
    MostViewedCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"Cell" forIndexPath:indexPath];
    
    [cell.productImageView sd_setImageWithURL:[NSURL URLWithString:[productsImageArr objectAtIndex:indexPath.row]]placeholderImage:[UIImage imageNamed:@"men"] completed:nil];
    
    cell.productNameLabel.text = [productsNameArr objectAtIndex:indexPath.row];
    cell.presentAmountLabel.text = [NSString stringWithFormat:@"%@",[productPriceArr objectAtIndex:indexPath.row]];
    
    
    cell.productRatingView.value = [[ratingArr objectAtIndex:indexPath.row]intValue];
    cell.productRatingView.userInteractionEnabled = NO;
    
    
    return cell;
    
}


-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath
{
  
    if(collectionView == _mostViewedCollectionView)
    {
        ProductInfoViewController *productsList = [self.storyboard instantiateViewControllerWithIdentifier:@"ProductInfoViewController"];
        [productsList productInfoServiceCall:[productIdArr objectAtIndex:indexPath.row]];
        [self.navigationController pushViewController:productsList animated:YES];
        
    }
}

- (CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout*)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    
    //if (collectionView == _viewAllCollectionView) {
    CGFloat width = (CGFloat) (_mostViewedCollectionView.frame.size.width/2);
    
    return CGSizeMake(width-5,240);
}

- (IBAction)backBtn:(id)sender {
    
    [self.navigationController popViewControllerAnimated:YES];
    
    
}
@end
