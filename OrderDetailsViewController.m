//
//  OrderDetailsViewController.m
//  Noura App
//
//  Created by volive solutions on 21/10/17.
//  Copyright © 2017 volive solutions. All rights reserved.
//

#import "OrderDetailsViewController.h"
#import "AppDelegate.h"
#import "SharedClass.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "SVProgressHUD.h"
#import "OrderDetailsCell.h"

@interface OrderDetailsViewController ()
{
    OrderDetailsCell *ordersCell;
    NSMutableArray *productNameArray;
    NSMutableArray *productImageArray;
    NSMutableArray *productAmountArray;
    NSMutableArray *descriptionArray;
    NSMutableArray *productsArrayCount;
    NSMutableArray *dataArrayCount;
    NSMutableArray *addressArrayCount;
    NSString *addressString;
    
    NSString *ordersString;
}

@end

@implementation OrderDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    [self loadViewWithCustomDesign];
    // Do any additional setup after loading the view.
}

-(void)loadViewWithCustomDesign {
    self.navigationItem.title = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Order Details"];
    productNameArray = [NSMutableArray new];
    productImageArray = [NSMutableArray new];
    productAmountArray = [NSMutableArray new];
    descriptionArray = [NSMutableArray new];
    
    productsArrayCount = [NSMutableArray new];
    dataArrayCount = [NSMutableArray new];
    addressArrayCount = [NSMutableArray new];

    
    _shippingToLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Shipping To"];
    _yourOrdersLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"YOUR ORDERS"];
    _paymentSummeryLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Payment Summary"];
    
    _subTotalLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Sub Total"];
    _shippingLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Shipping Amount"];
    _grandTotalLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Grand Total"];
    _orderLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Order Id"];
    _statusNameLabel.text = [[SharedClass sharedInstance] languageSelectedStringForKey:@"Status"];
    
//    UIView *view = [[UIView alloc]initWithFrame:CGRectMake(0, 0, 257, 43)];
//    view.backgroundColor = [UIColor clearColor];
//    
//    UIImageView *imagev = [[UIImageView alloc]initWithFrame:CGRectMake(5, 7, 80, 25)];
//    imagev.image = [UIImage imageNamed:@""];
//    [view addSubview:imagev];
//    self.navigationItem.titleView = view;
//

}


-(void)orderDetailsServiceCall:(NSString *)orderId
{
    NSString * languageStr=[[NSUserDefaults standardUserDefaults]objectForKey:@"language"];
    [SVProgressHUD setForegroundColor:[UIColor colorWithRed:245.0f/255.0f green:108.0f/255.0f blue:125.0f/255.0f alpha:1]];
    [SVProgressHUD setBackgroundColor:[UIColor grayColor]];
    [SVProgressHUD showWithStatus:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Loading..."]];
    
    //http://voliveafrica.com/noura_services/services/order_information?store_id=1&order_id=100000008
    ordersString = orderId;
    
    NSMutableDictionary *ordersPostDictionary = [[NSMutableDictionary alloc]init];
    
    
    //[ordersPostDictionary setObject:[[NSUserDefaults standardUserDefaults]objectForKey:@"orderIdArray"] forKey:@"order_id"];
    [ordersPostDictionary setObject:orderId forKey:@"order_id"];
    [ordersPostDictionary setObject:languageStr forKey:@"store_id"];
    //[ordersPostDictionary setObject:@"1" forKey:@"store_id"];
    [[SharedClass sharedInstance]fetchResponseforParameter:@"order_information?" withPostDict:ordersPostDictionary andReturnWith:^(NSData *dataFromJson) {
        
        NSDictionary * dataDictionary = (NSDictionary *)(dataFromJson);
        
        NSString * status = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"status"]];
       // NSString * message = [NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]];
        NSLog(@"%@",dataFromJson);
        NSLog(@"%@ My Data Is",dataDictionary);
        
        if ([status isEqualToString:@"1"])
        {
            dispatch_async(dispatch_get_main_queue(), ^{
                
                    [SVProgressHUD dismiss];
                
            });
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                productsArrayCount=[dataDictionary objectForKey:@"products"];
                
                dataArrayCount = [dataDictionary objectForKey:@"data"];
                addressArrayCount = [dataDictionary objectForKey:@"address"];
                
                if (productsArrayCount.count>0) {
                    for (int i=0; i<productsArrayCount.count; i++) {
                        [productImageArray addObject:[[[dataDictionary objectForKey:@"products"]objectAtIndex:i]objectForKey:@"image"]];
                        [productNameArray addObject:[[[dataDictionary objectForKey:@"products"]objectAtIndex:i]objectForKey:@"name"]];
                        [productAmountArray addObject:[[[dataDictionary objectForKey:@"products"]objectAtIndex:i]objectForKey:@"price"]];
                        
                        //[descriptionArray addObject:[[dataDictionary objectForKey:@"products"]objectForKey:@"short_description"]];
                        
                        NSLog(@"%@",productNameArray);
                         //NSLog(@"%@",_clothsList);
                    }
                }
                NSLog(@"%@",productsArrayCount);
                NSLog(@"%@",dataArrayCount);
                NSLog(@"%@",addressArrayCount);
                [_orderDetailsTableView reloadData];
            });
            dispatch_async(dispatch_get_main_queue(), ^{
                
                _subTotalAmountLabel.text = [[dataDictionary objectForKey:@"data"]objectForKey:@"subtotal"];
                _shippingAmountLabel.text = [[dataDictionary objectForKey:@"data"]objectForKey:@"shipping_amount"];
                _grandTotalAmountLabel.text = [[dataDictionary objectForKey:@"data"]objectForKey:@"grand_total"];
                _orderIdLabel.text = [[dataDictionary objectForKey:@"data"]objectForKey:@"order_id"];
                _statusLabel.text = [[dataDictionary objectForKey:@"data"]objectForKey:@"status"];
                
                });
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                addressString=[NSString stringWithFormat:@"%@ ,%@, %@, %@,%@, %@",
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"firstname"],
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"lastname"],
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"street"],
                               
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"city"],
                               //[[dataDictionary objectForKey:@"address"]objectForKey:@"postcode"],
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"country_id"],
                               [[dataDictionary objectForKey:@"address"]objectForKey:@"telephone"]];
                NSLog(@"Address is %@",addressString);
                _addressLabel.text = addressString;
                
            });

        }else {
            
            dispatch_async(dispatch_get_main_queue(), ^{
                
                
                [[SharedClass sharedInstance]showAlertWithTitle:[[SharedClass sharedInstance]languageSelectedStringForKey:@"Alert!"] withMessage:[NSString stringWithFormat:@"%@",[dataDictionary objectForKey:@"message"]] onViewController:self completion:^{ }];
            });
        }
        
    }];

}


-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return productNameArray.count;
}
-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    ordersCell  = [_orderDetailsTableView dequeueReusableCellWithIdentifier:@"OrderDetailsCell" forIndexPath:indexPath];
    ordersCell.productNameLabel.text = [productNameArray objectAtIndex:indexPath.row];
    [ordersCell.productImageView sd_setImageWithURL:[NSURL URLWithString:[productImageArray objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@""]];
    ordersCell.productAmountLabel.text = [productAmountArray objectAtIndex:indexPath.row];
    
    return  ordersCell;
}






/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

- (IBAction)backBtn:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}
@end
